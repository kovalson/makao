# Description

Implementation of card game "Makao" (also known as "Macau") in client-server architecture. The server was written in TypeScript using `socket.io` library while client app was implemented using typed `React.js`.

# Installation

## Prerequisites

You will require `Node.js` to run this program. Also `npm` is highly recommended in order to easily install all dependencies and packages.

## Installation

To install, clone this repository, then in both `/clients/` and `/server/` directories open consoles (or terminals) and type:
```
npm install
```

When all required packages are downloaded, in both terminals type:
```
npm start
```

If you want to compile server on file save, run:
```
npm run start:watch
```
as described in `package.json`.

# Game rules

## Preliminary

Game uses standard deck (52 cards). Before the play the deck is shuffled and each player (one after the other) is given a card from the deck until all of them have five cards in their hands. Players shan't show their cards to anyone. After that, one card is drawn from the deck and put flipped visible on the stack. Then the game begins.

## Goals

**The main goal of the course is to dispose of all cards in ones hand**. Each player that got rid of his cards becomes a winner. In the end of the game there are always at most $`n - 1`$ winners where $`n`$ is the number of players participating. The last player cannot be a winner as the game stops as soon as $`n - 1^{th}`$ player becomes one.

## Gameplay

Gameplay consists of players turns. Depending on current game state, and only during their turn, players can perform the actions below:

- draw a card (**draw**),
- play a card (**play**),
- pass turn (**pass**).

Playing different cards may cause different effects on other players or on the game stack depending on type of the card and current game state. All states as well as card types are listed and described below.

*By default, player can, during his turn, either play a card or first draw a card and then play any other card in his hand. This rule is global for the game and can be changed by any game state. For example, if player has a card that is allowed to be played, he might (but doesn't have to) play that card as well as he could draw a card from the deck and play that drawn card (as long as it is also allowed to be played). However, one can only play one card during his turn.*

## Game states

The are four game states. Each state specifies which of the actions can be performed by players and when. Each state has its own card type that can be used during that state.

### Table of allowance

| State | Cards allowed | Actions allowed |
|-|-|-|
| `Regular` | all | **draw**, **play**, **pass**
| `Skip` | $`4`$ of any suit | **play**, **pass**
| `Rank-demanding` | either $`J`$ of any suit or card of demanded rank | **play**, **draw**
| `Combat` | $`2`$, $`3`$ of any suit, $`K`$ of hearts, $`K`$ of spades | **play**, **draw**

### State rules

Each game state comes with instructions and special rules corresponding certain actions that are allowed. The rules are as follows:

- Global rules (*any of these might be changes by the game state*):
  - players may play only one card from their hand,
  - playing a card causes player to automatically pass his turn,
  - players may draw only one card from the deck,
  - player cannot draw a card after playing a card,
  - if player cannot draw a card from the deck due to lack of cards, and the deck cannot be refilled from the stack (due to lack of cards) then the player may pass his turn. In such case, unless stated differently, the game state changes to `Regular`.
- `Regular state`:
  - player may draw only one card provided he hasn't done that during his turn,
  - player may pass his turn only if he has drawn a card. If there are no cards left on the deck and the stack has no cards to build deck from, player may pass his turn without playing or drawing any card,
  - playing a card of any other type than `Regular` (i.e. `Skip`, `Rank-demanding` or `Combat`) causes the game state to change to that type.
- `Skip state`:
  - during that state players are only allowed to play `Skip` cards,
  - playing a `Skip` card causes the game state to remain,
  - if a player doesn't have `Skip` card in his hand he must pass his turn,
  - player that passed his turn without playing a card must skip next $`T`$ turns where $`T`$ is the number of `Skip` cards played during that `Skip` game state. The game state then changes to `Regular`,
  - players cannot draw cards during the `Skip` game state,
  - players cannot play a `Skip` card on themselves. In such situation the game state changes to `Regular` despite playing a `Skip` card (`Skip` card itself can be played but doesn't work as normal)\*,
- `Rank-demanding state`:
  - player $`X`$ that played a `Rank-demanding` card states which rank he demands,
  - ranks that can be demanded are: $`5`$, $`6`$, $`7`$, $`8`$, $`9`$ and $`10`$. No other ranks can be demanded,
  - suit of demanded rank is irrelevant no matter what suit is on top of the stack, i.e. any card that meets rank requirement can be played,
  - player might draw a card from the deck,
  - player might not play a card even if he meets the rank requirement with respect to the rule that he cannot pass his turn until he draws or plays a card,
  - the `Rank-demanding` state remains until the end of $`X`$ turn, then it changes to `Regular`,
  - if any other player plays a `Rank-demanding` card during that state, he becomes the $`X`$ player, i.e. the state remains until his turn is over
  - player that played another `Rank-demanding` card may change the demand to any rank listed above as allowed.
- `Combat state`:
  - during that state players are only allowed to play `Combat` cards,
  - the player $`X`$ that started `Combat` state causes the next player in turn to draw total amount $`T`$ of $`C`$ cards, where $`C`$ is the *combat value* of the card (combat values are listed below),
  - if another player plays `Combat` card, he adds the *combat value* of that card to the total amount $`T`$ of cards to be drawn,
  - player may play a `Combat` card as long as it has the same rank or suit as the card on top of the stack,
  - player may draw a card during that state, then he might play that card or any other `Combat` card from his hand with respect to previous rule,
  - player that passed turn without playing a card has to draw $`T`$ cards from the deck.

*\* That situation might happen if player $`X`$ plays a `Skip` card while all other players already skip their turns. It results in the `Skip` state returning (after a round) to $`X`$ which would have to skip next turn(s) because of playing the card himself.*

## Cards

### Card types

Each game state has its own card type. There is also one additional type that doesn't come with corresponding game state. Default configuration of card types is as follows:

- `Regular`: $`5`$, $`6`$, $`7`$, $`8`$, $`9`$, $`10`$ and $`Q`$ of any suit, $`K`$ of diamonds and $`K`$ of clubs,
- `Skip`: $`4`$ of any suit,
- `Rank-demanding`: $`J`$ of any suit,
- `Combat`: $`2`$ and $`3`$ of any suit, $`K`$ of hearts, $`K`$ of spades,
- `Suit-changing`: $`A`$ of any suit.

Playing card of each type causes the game state to change to that type. One exception is the `Suit-changing` card which is only accepted during `Regular` game state and doesn't change it. When player plays a `Suit-changing` card, he may change the suit of that card to any other suit after playing that card. Then the game continues in `Regular` state.

### Combat values

| Card | Combat value |
|-|:-:|
| $`2`$ of any suit | 2 |
| $`3`$ of any suit | 3 |
| $`K`$ of hearts | 5 |
| $`K`$ of spades | 5 |

## Other information

Though in classic Makao any number of players can join, it is recommended that only up to 4 players participate at most. Current implementation supports 2-4 players game rooms. Any configuration above the upper boundary could cause the game to become unplayable or at least unsatisfiable.

# Features to implement

- [X] Working game.
- [ ] "$`5`$ to all and all to $`5`$" - during `Regular` game state allow playing $`5`$ of any suit no matter what the top stack card is.
- [ ] Allow playing also 3 or 4 cards of the same rank or suit at the same time.
- [ ] Add *blocking cards* as new card type. *Blocking card* stops the `Combat` state and immediately changes it back to `Regular`.
- [ ] Set $`Q`$ of spades as *blocking card*.