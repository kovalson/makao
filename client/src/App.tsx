import * as React from "react";

import AppViews from "./class/enum/AppViews";
import ConnectingToServer from "./components/appViews/ConnectingToServer";
import ConnectionLost from "./components/appViews/ConnectionLost";
import ClientName from "./components/appViews/ClientName";
import RoomsList from "./components/appViews/RoomsList";
import Alert from "./components/Alert";
import Socket from "./Socket";
import ResponseMessage from "./class/ResponseMessage";
import RoomCreation from "./components/appViews/RoomCreation";
import TheRoom from "./components/appViews/TheRoom";
import TheGame from "./components/appViews/TheGame";

interface IAlert {
  display: boolean,
  message: ResponseMessage
}

interface IState {
  clientName: string,
  appView: AppViews,
  alert: IAlert
}

class App extends React.Component<{}, IState> {
  state = {
    clientName: "",
    appView: AppViews.ConnectingToServer,
    alert: {
      display: false,
      message: ResponseMessage.Success
    }
  };

  componentDidMount() {
    Socket.on("connect", () => {
      this.setState({
        appView: AppViews.ClientName
        // appView: AppViews.TheGame
      });
    });

    Socket.on("connect_timeout", () => {
      this.setState({
        clientName: "",
        appView: AppViews.ConnectionLost
      });
    });

    Socket.on("connect_error", () => {
      this.setState({
        clientName: "",
        appView: AppViews.ConnectionLost
      });
    });
  }

  setClientNameAndSetView = (name: string, view: AppViews) => {
    this.setState({
      clientName: name,
      appView: view
    });
  };

  setAppView = (view: AppViews) => {
    this.setState({
      appView: view
    })
  };

  displayAlert = (message: ResponseMessage) => {
    this.setState({
      alert: {
        display: true,
        message: message
      }
    });
  };

  onAlertClose = () => {
    this.setState({
      alert: {
        display: false,
        message: ResponseMessage.Success
      }
    });
  };

  render() {
    const {appView, alert} = this.state;

    return (
      <React.Fragment>

        {appView === AppViews.ConnectingToServer &&
          <ConnectingToServer />}
        
        {appView === AppViews.ConnectionLost &&
          <ConnectionLost />}
        
        {appView === AppViews.ClientName &&
          <ClientName
            displayAlert={this.displayAlert}
            setClientNameAndSetView={this.setClientNameAndSetView} />}
          
        {appView === AppViews.RoomsList &&
          <RoomsList
            setAppView={this.setAppView}
            displayAlert={this.displayAlert} />}
        
        {appView === AppViews.RoomCreation &&
          <RoomCreation
            setAppView={this.setAppView}
            displayAlert={this.displayAlert} />}
        
        {appView === AppViews.TheRoom &&
          <TheRoom
            setAppView={this.setAppView}
            displayAlert={this.displayAlert}
            clientName={this.state.clientName} />}

        {appView === AppViews.TheGame &&
          <TheGame
            setAppView={this.setAppView}
            displayAlert={this.displayAlert}
            clientName={this.state.clientName} />}
        
        {alert.display &&
          <Alert
            message={alert.message}
            onClose={this.onAlertClose} />}

      </React.Fragment>
    );
  }
}

export default App;