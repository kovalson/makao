import * as React from "react";
import { Form, Tag, Icon } from "react-bulma-components";
import ReactTooltip from "react-tooltip";

import RoomDetails from "../../class/RoomDetails";

interface IProps {
  room: RoomDetails
}

const RoomInfo: React.StatelessComponent<IProps> = ({ room }) => {
  const players: string = (room.getCurrentPlayersNumber() + " / " + room.getPlayersNumber());
  const owner: string = room.getOwner();

  return (
    <Form.Field kind="group">

      <Form.Control data-tip data-for="players-tooltip">
        <Tag.Group gapless>
          <Tag size="medium" color="light">
            <Icon>
              <i className="fas fa-user"></i>
            </Icon>
          </Tag>
          <Tag size="medium" color="light">
            {players}
          </Tag>
        </Tag.Group>
      </Form.Control>
      <ReactTooltip id="players-tooltip" effect="solid">
        <span>
          Number of players
        </span>
      </ReactTooltip>

      <Form.Control data-tip data-for="owner-tooltip">
        <Tag.Group gapless>
          <Tag size="medium" color="light">
            <Icon>
              <i className="fas fa-user-edit"></i>
            </Icon>
          </Tag>
          <Tag size="medium" color="light">
            {owner}
          </Tag>
        </Tag.Group>
      </Form.Control>
      <ReactTooltip id="owner-tooltip" effect="solid">
        <span>
          Room created by {owner}
        </span>
      </ReactTooltip>

    </Form.Field>
  );
};

export default RoomInfo;