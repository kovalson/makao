import * as React from "react";
import { Table } from "react-bulma-components";

import RoomDetails from "../../class/RoomDetails";
import Player from "../../class/Player";
import PlayerElement from "./PlayerElement";
import RemainingElement from "./RemainingElement";

interface IProps {
  room: RoomDetails,
  clientName: string
}

const renderPlayer = (player: Player, index: number, clientName: string, isClientOwner: boolean) => {
  return (
    <PlayerElement
      key={index}
      index={index + 1}
      name={player.getName()}
      control={player.getControl()}
      readyToPlay={player.isReadyToPlay()}
      clientName={clientName}
      isClientOwner={isClientOwner} />
  );
};

const renderRemainingElements = (elements: number, isClientOwner: boolean) => {
  let array: JSX.Element[] = [];
  while (elements--) {
    array.push(
      <RemainingElement
        key={elements}
        index={elements}
        isClientOwner={isClientOwner} />
    );
  }
  return array;
};

const PlayersTable: React.StatelessComponent<IProps> = ({ room, clientName }) => {
  const remainingElements = (room.getPlayersNumber() - room.getCurrentPlayersNumber());
  const isClientOwner = (room.getOwner() === clientName);
  return (
    <Table striped={false}>
      <thead>
        <tr>
          <th colSpan={5}>
            Players list
          </th>
        </tr>
      </thead>
      <tbody>
        {room.getPlayers().map((player, index) => renderPlayer(player, index, clientName, isClientOwner))}
        {(remainingElements > 0) && renderRemainingElements(remainingElements, isClientOwner)}
      </tbody>
    </Table>
  );
};

export default PlayersTable;