import * as React from "react";
import { Tag, Button, Icon } from "react-bulma-components";
import ReactTooltip from "react-tooltip";

import Socket from "../../Socket";
import ClientMessage from "../../class/enum/ClientMessage";

interface IProps {
  index: number,
  name: string,
  control: string,
  readyToPlay: boolean,
  clientName: string,
  isClientOwner: boolean
}

const getIcon = (name: string, control: string, id: number, isOwner: boolean, isSelf: boolean) => {
  const icon = (control === "Bot" ? "fas fa-robot" : "fas fa-user");
  const text = (control === "Bot" ? "Controlled by bot" : "Controlled by human");
  if (isOwner && !isSelf) {
    return (
      <div>
      <Button
        size="small"
        color="danger"
        data-tip
        data-for={"remove-player-button-" + id}
        onClick={() => handleRemoveFromRoom(name)}>
        <Icon>
          <i className={icon}></i>
        </Icon>
        <Icon>
          <i className="fas fa-minus"></i>
        </Icon>
      </Button>
      <ReactTooltip id={"remove-player-button-"+ id} effect="solid">
        <p>{text}</p>
        <p>Remove from room</p>
      </ReactTooltip>
    </div>
    );
  } else {
    return (
      <div>
        <i className={icon}></i>
        <ReactTooltip id={"icon-"+ id} effect="solid">
          <span>
            {text}
          </span>
        </ReactTooltip>
      </div>
    );
  }
};

const handleRemoveFromRoom = (name: string) => {
  Socket.emit(ClientMessage.RemoveFromRoom, name);
};

const getStatus = (readyToPlay: boolean) => {
  if (readyToPlay) {
    return (<Tag color="success">ready</Tag>);
  } else {
    return (<Tag>awaiting</Tag>);
  }
};

const PlayerElement: React.StatelessComponent<IProps> = (props) => {
  const { index, name, control, readyToPlay, isClientOwner, clientName } = props;
  const isSelf: boolean = (clientName === name);

  return (
    <tr>
      <td className="has-text-centered">{index}</td>
      <td>{name}</td>
      <td className="has-text-centered" data-tip data-for={"icon-"+ index}>{getIcon(name, control, index, isClientOwner, isSelf)}</td>
      <td className="has-text-centered">{getStatus(readyToPlay)}</td>
    </tr>
  );
};

export default PlayerElement;