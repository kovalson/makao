import * as React from "react";
import { Button, Icon } from "react-bulma-components";
import ReactTooltip from "react-tooltip";

import Socket from "../../Socket";
import ClientMessage from "../../class/enum/ClientMessage";

interface IProps {
  index: number,
  isClientOwner: boolean
}

const handleAddBot = () => {
  Socket.emit(ClientMessage.AddBot);
};

const renderAddBotButton = (index: number, isClientOwner: boolean) => {
  if (isClientOwner) return (
    <div>
      <Button
        size="small"
        data-tip
        data-for={"add-bot-button-" + index}
        onClick={handleAddBot}>
        <Icon>
          <i className="fas fa-robot"></i>
        </Icon>
        <Icon>
          <i className="fas fa-plus"></i>
        </Icon>
      </Button>
      <ReactTooltip id={"add-bot-button-"+ index} effect="solid">
        <span>
          Add bot
        </span>
      </ReactTooltip>
    </div>
  );
};

const RemainingElement: React.StatelessComponent<IProps> = ({ index, isClientOwner }) => {
  return (
    <tr className="has-background-light">
      <td></td>
      <td>Slot open</td>
      <td className="has-text-centered">
        {renderAddBotButton(index, isClientOwner)}
      </td>
      <td></td>
    </tr>
  );
};

export default RemainingElement;