import * as React from "react";
import { Button, Icon } from "react-bulma-components";

import Socket from "../../Socket";
import RoomsListElement from "../../class/RoomsListElement";
import ClientMessage from "../../class/enum/ClientMessage";

interface IProps {
  index: number,
  room: RoomsListElement
}

const RoomElement: React.StatelessComponent<IProps> = ({ room, index }) => {
  const name = room.getName();
  const playersNumber = room.getPlayersNumber();
  const currentPlayersNumber = room.getCurrentPlayersNumber();
  const owner = room.getOwner();
  const roomSocket = room.getSocket();
  const state = room.getState();

  return (
    <tr>
      <td className="has-text-centered">{index + 1}</td>
      <td>{name}</td>
      <td className="has-text-centered">{currentPlayersNumber} / {playersNumber}</td>
      <td className="has-text-centered">{owner}</td>
      <td className="has-text-centered">{state}</td>
      <td className="has-text-centered">
        <Button
          color="success"
          size="small"
          onClick={() => Socket.emit(ClientMessage.JoinRoom, roomSocket)}>
          <Icon>
            <i className="fas fa-angle-right"></i>
          </Icon>
        </Button>
      </td>
    </tr>
  );
};

export default RoomElement;