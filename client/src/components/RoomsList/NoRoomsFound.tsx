import * as React from "react";
import {
  Notification,
  Columns
} from "react-bulma-components";

function NoRoomsFound() {
  return (
    <Notification color="white">
      <Columns gapless>
        <Columns.Column size={1} className="is-hidden-mobile" textAlignment="centered">
          <i className="fas fa-info-circle"></i>
        </Columns.Column>
        <Columns.Column>
          <strong>No rooms found.</strong>
          <p>You can create your own room by clicking the button below.</p>
        </Columns.Column>
      </Columns>
    </Notification>
  );
}

export default NoRoomsFound;