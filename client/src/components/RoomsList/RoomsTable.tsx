import * as React from "react";
import {
  Table,
  Icon
} from "react-bulma-components";
import ReactTooltip from "react-tooltip";

import RoomElement from "./RoomElement";
import RoomsListElement from "../../class/RoomsListElement";

interface IProps {
  rooms: Array<RoomsListElement>
}

const renderRoom = (room: RoomsListElement, index: number) => {
  return (
    <RoomElement
      key={room.getSocket()}
      room={room}
      index={index} />
  );
};

const RoomsTable: React.StatelessComponent<IProps> = ({ rooms }) => {
  return (
    <Table striped size="fullwidth" className="is-hoverable">
      <thead>
        <tr>
          <th className="has-text-centered">No.</th>
          <th>Name</th>
          <th className="has-text-centered" data-tip data-for="tooltipHumans">
            <Icon>
              <i className="fas fa-user"></i>
            </Icon>
            <ReactTooltip id="tooltipHumans" effect="solid">
              <span>
                Players number
              </span>
            </ReactTooltip>
          </th>
          <th className="has-text-centered">Owner</th>
          <th className="has-text-centered">State</th>
          <th className="has-text-centered">Join</th>
        </tr>
      </thead>
      <tbody>
        {rooms.map(renderRoom)}
      </tbody>
    </Table>
  );
};

export default RoomsTable;