import * as React from "react";
import {
  Hero,
  Loader,
  Columns,
  Container,
  Heading
} from "react-bulma-components";

function ConnectionLost() {
  return (
    <Hero size="fullheight">
      <Hero.Body>
        <Container>
          
          <Columns centered>
            <Loader style={{
              width: 150,
              height: 150,
              border: "10px solid #afafaf",
              borderTopColor: "transparent",
              borderRightColor: "transparent"
            }} />
          </Columns>

          <Heading textAlignment="centered">
            <p>Connection lost.</p>
            <p>Trying to reconnect...</p>
          </Heading>

        </Container>
      </Hero.Body>
    </Hero>
  );
};

export default ConnectionLost;