import * as React from "react";
import { Hero, Container, Columns } from "react-bulma-components";

import PlayerCards from "../TheGame/PlayerCards";
import GameCard from "../../class/Card";
import Deck from "../TheGame/Deck";
import Stack from "../TheGame/Stack";
import Socket from "../../Socket";
import ServerMessage from "../../class/enum/ServerMessage";
import ClientMessage from "../../class/enum/ClientMessage";
import CurrentPlayer from "../TheGame/CurrentPlayer";
import PassTurnButton from "../TheGame/PassTurnButton";

import "../../scss/the-game.scss";
import UpdateAction from "../../class/UpdateAction";
import AppViews from "../../class/enum/AppViews";
import CardType from "../../class/enum/CardType";
import SuitChangeDialog from "../TheGame/SuitChangeDialog";
import Suit from "../../class/enum/Suit";
import RankDemandDialog from "../TheGame/RankDemandDialog";

interface IProps {
  setAppView: Function,
  displayAlert: Function,
  clientName: string
}

interface IState {
  actions: Array<UpdateAction>,

  deck: Array<GameCard>,
  stack: Array<GameCard>,
  cards: Array<GameCard>,
  drawnCard: GameCard,

  hasTurn: boolean,
  currentPlayer: string,
  canPassTurn: boolean,
  canDrawCard: boolean,
  hasDrawnCard: boolean,
  
  isLoading: boolean,
  showSuitChangeDialog: boolean,
  suitChangeCard: GameCard,
  showRankDemandDialog: boolean,
  rankDemandCard: GameCard
}

class TheGame extends React.Component<IProps, IState> {
  private actionTimeout: number;
  private updateTimeout: number;
  private duringAction: boolean;

  constructor(props: IProps) {
    super(props);
    this.actionTimeout = 0;
    this.updateTimeout = 0;
    this.duringAction = false;
  }

  state = {
    actions: [],

    deck: [],
    stack: [],
    cards: [],
    drawnCard: GameCard.Empty(),
    
    hasTurn: false,
    currentPlayer: "",
    canPassTurn: false,
    canDrawCard: false,
    hasDrawnCard: false,

    isLoading: false,
    showSuitChangeDialog: false,
    suitChangeCard: GameCard.Empty(),
    showRankDemandDialog: false,
    rankDemandCard: GameCard.Empty()
  };
  
  componentDidMount() {
    Socket.on(ServerMessage.GameUpdate, (actions: Array<UpdateAction>) => {
      // console.log(actions);
      this.setState({
        actions: [
          ...this.state.actions,
          ...actions
        ]
      }, this.executeAction);
    });
  }

  componentWillUnmount() {
    Socket.off(ServerMessage.GameUpdate);
    clearTimeout(this.actionTimeout);
    clearTimeout(this.updateTimeout);
  }

  executeAction = () => {
    if (this.actionTimeout || this.updateTimeout || this.duringAction || this.state.actions.length < 1) {
      return;
    }
    
    this.duringAction = true;
    const firstAction = [...this.state.actions].shift(); 
    if (!firstAction) {
      this.duringAction = false;
      return;
    };
    const action: UpdateAction = firstAction;

    switch (action.type) {
      case ServerMessage.DeckUpdate:
        this.handleDeckUpdate(action.data);
        break;
      
      case ServerMessage.StackUpdate:
        this.handleStackUpdate(action.data);
        break;
      
      case ServerMessage.PlayerCards:
        this.handleAddPlayerCards(action.data);
        break;
      
      case ServerMessage.CardsUpdate:
        this.handleCardsUpdate(action.data);
        break;
      
      case ServerMessage.TurnUpdate:
        this.handleTurnUpdate(action.data);
        break;
      
      case ServerMessage.PlayerUpdate:
        this.handlePlayerUpdate(action.data);
        break;
      
      case ServerMessage.CardDrawn:
        this.handleCardDrawn(action.data);
        break;
      
      case ServerMessage.ChangeStackCardSuit:
        this.handleChangeStackCardSuit();
        break;
      
      case ServerMessage.ShowCardDemand:
        this.handleShowCardDemand();
        break;
      
      case ServerMessage.Win:
        this.handlePlayerWin(action.data);
        break;
      
      case ServerMessage.GameOver:
        this.handleGameOver();
        break;
      
      default:
        console.log("Action unknown:", action);
        this.executeNextAction();
        break;
    }
  };

  handleDeckUpdate = (update: any) => {
    const {type, amount} = update;
    switch (type) {
      case "set":
        this.setState({
          deck: Array.from({ length: amount }, () => GameCard.Empty())
        }, this.executeNextAction);
        break;
      
      case "remove":
        this.setState({
          deck: Array.from({ length: this.state.deck.length - amount }, () => GameCard.Empty())
        }, this.executeNextAction);
        break;
      
      case "add":
        this.setState({
          deck: [
            ...this.state.deck,
            ...Array.from({ length: amount }, () => GameCard.Empty())
          ]
        }, this.executeNextAction)
        break;
        
      default:
        // this.executeNextAction();
        break;
    }
  };

  handleStackUpdate = (update: any) => {
    const {type, cards} = update;
    switch (type) {
      case "set":
        this.setState({
          stack: cards.map((object: any) => GameCard.fromObject(object))
        }, this.executeNextAction);
        break;
      
      case "add":
        this.setState({
          stack: [
            ...this.state.stack,
            ...cards.map((object: any) => GameCard.fromObject(object))
          ]
        }, this.executeNextAction);
        break;
      
      case "remove":
        this.setState({
          stack: [this.state.stack[this.state.stack.length - 1]]
        }, this.executeNextAction);
        break;
      
      default:
        break;
    }
  };

  handleAddPlayerCards = (array: any) => {
    const cards: Array<GameCard> = array.map((object: any) => GameCard.fromObject(object));
    const addDelay = 200;
    const game = this;
    (function addCard(cards: Array<GameCard>) {
      if (cards.length === 0) return;
      const newCards = cards;
      const shift = newCards.shift();
      if (!shift) return;
      const card: GameCard = shift;
      game.setState({
        cards: [
          ...game.state.cards,
          card
        ]
      });
      if (newCards.length === 0) {
        game.executeNextAction();
      } else {
        game.updateTimeout = window.setTimeout(() => addCard(newCards), addDelay);
      }
    })(cards);
  };

  handleCardsUpdate = (update: any) => {
    const {type, cards, card} = update;
    switch (type) {
      case "add":
        this.setState({
          cards: [
            ...this.state.cards,
            ...cards.map((object: any) => GameCard.fromObject(object))
          ]
        }, this.executeNextAction);
        break;
      
      case "remove":
        this.setState({
          cards: this.state.cards.filter((gameCard: GameCard) => {
            return !(
              gameCard.getRank() === card.rank &&
              gameCard.getSuit().toString() === card.suit
            );
          })
        }, this.executeNextAction);
        break;
      
      case "set":
        this.setState({
          cards: cards.map((object: any) => GameCard.fromObject(object))
        }, this.executeNextAction);
        break;
      
      default:
        break;
    }
  };

  handleTurnUpdate = (name: string) => {
    this.setState({
      currentPlayer: name
    }, () => this.executeNextAction(800));
  };

  handlePlayerUpdate = (update: any) => {
    // console.log(update);
    this.setState({
      isLoading: false,
      hasTurn: update.hasTurn,
      hasDrawnCard: update.drawnCard,
      canPassTurn: update.canPassTurn,
      canDrawCard: update.canDrawCard
    }, this.executeNextAction);
  };

  handleCardDrawn = (cardObject: any) => {
    this.setState({
      drawnCard: GameCard.fromObject(cardObject)
    }, () => this.executeNextAction(800));
  };
  
  handleChangeStackCardSuit = () => {
    const newStack: Array<GameCard> = this.state.stack;
    newStack[newStack.length - 1].hideSuit();
    this.setState({
      stack: newStack
    }, () => {
      this.updateTimeout = window.setTimeout(() => {
        const newStack: Array<GameCard> = this.state.stack;
        newStack[newStack.length - 1].applyChangedSuit();
        newStack[newStack.length - 1].showSuit();
        
        clearTimeout(this.updateTimeout);
        this.updateTimeout = 0;

        this.setState({
          stack: newStack
        }, this.executeNextAction);
      }, 800);
    });
  };

  handleShowCardDemand = () => {
    const newStack: Array<GameCard> = this.state.stack;
    newStack[newStack.length - 1].hideSuit();
    this.setState({
      stack: newStack
    }, () => {
      this.updateTimeout = window.setTimeout(() => {
        const newStack: Array<GameCard> = this.state.stack;
        newStack[newStack.length - 1].applyRankDemand();
        newStack[newStack.length - 1].showSuit();
        
        clearTimeout(this.updateTimeout);
        this.updateTimeout = 0;

        this.setState({
          stack: newStack
        }, this.executeNextAction);
      }, 800);
    });
  };

  handlePlayerWin = (name: string) => {
    const previousState = this.state;
    previousState.currentPlayer = name + " wins";
    
    this.setState({
      isLoading: true,
      hasTurn: false,
      hasDrawnCard: false,
      currentPlayer: name + " wins"
    }, () => {
      let gameOver: boolean = false;

      const nextAction: UpdateAction = this.state.actions[1];
      if (nextAction) {
        const type = nextAction.type;
        if (type === ServerMessage.GameOver) {
          gameOver = true;
        }
      }

      if (!gameOver) {
        this.updateTimeout = window.setTimeout(() => {
          this.setState(previousState, () => {
            clearTimeout(this.updateTimeout);
            this.updateTimeout = 0;
            this.executeNextAction();
          });
        }, 1000);
      } else {
        this.executeNextAction(1500);
      }
    });
  };

  handleGameOver = () => {
    this.setState({
      isLoading: true,
      hasTurn: false,
      hasDrawnCard: false,
      currentPlayer: "Game Over"
    }, () => {
      this.actionTimeout = window.setTimeout(() => {
        this.props.setAppView(AppViews.TheRoom);
        Socket.emit(ClientMessage.GetRoomInfo);
      }, 2000);
    });
  };

  executeNextAction = (delay: number = 200) => {
    this.actionTimeout = window.setTimeout(() => {
      const newActions = this.state.actions;
      
      // shift action that just has been handled
      newActions.shift();

      this.setState({
        actions: newActions
      }, () => {
        this.duringAction = false;
        clearTimeout(this.actionTimeout);
        clearTimeout(this.updateTimeout);
        this.actionTimeout = 0;
        this.updateTimeout = 0;
        this.executeAction();
      });
    }, delay);
  };
  
  handleDrawCard = () => {
    const {hasTurn, hasDrawnCard, isLoading} = this.state;
    if (!hasTurn || hasDrawnCard || isLoading) {
      return;
    }

    this.setState({
      isLoading: true
    }, () => {
      Socket.emit(ClientMessage.DrawCard);
    });
  };

  handlePlayCard = (card: GameCard) => {
    const {hasTurn, isLoading} = this.state;
    if (!hasTurn || isLoading) {
      return;
    }

    if (card.isDisabled() || !card.isPlayable()) {
      return;
    }

    // TODO: show dialog selecting suit for suit-changing cards
    if (card.isType(CardType.SuitChanging)) {
      this.setState({
        showSuitChangeDialog: true,
        suitChangeCard: card
      });
    } else if (card.isType(CardType.RankDemanding)) {
      this.setState({
        showRankDemandDialog: true,
        rankDemandCard: card
      });
    } else {
      this.setState({
        hasTurn: false,
        isLoading: true
      }, () => {
        Socket.emit(ClientMessage.PlayCard, card.getSendable());
      });
    }
  };

  handlePassTurn = () => {
    const {canPassTurn, isLoading} = this.state;

    if (!canPassTurn || isLoading) {
      return;
    }

    this.setState({
      isLoading: true,
      hasTurn: false
    }, () => {
      Socket.emit(ClientMessage.PassTurn);
    });
  };

  closeSuitChangeDialog = () => {
    this.setState({
      showSuitChangeDialog: false
    });
  };

  handleClickSuitChangeCard = (card: GameCard, suit: Suit) => {
    card.setChangedSuit(suit);
    this.setState({
      hasTurn: false,
      isLoading: true,
      showSuitChangeDialog: false,
      suitChangeCard: GameCard.Empty()
    }, () => {
      Socket.emit(ClientMessage.PlayCard, card.getSendable());
    });
  };

  closeRankDemandDialog = () => {
    this.setState({
      showRankDemandDialog: false
    });
  };

  handleClickRankDemandCard = (card: GameCard, rank: number) => {
    card.setRankDemand(rank);
    this.setState({
      hasTurn: false,
      isLoading: true,
      showRankDemandDialog: false,
      rankDemandCard: GameCard.Empty()
    }, () => {
      Socket.emit(ClientMessage.PlayCard, card.getSendable());
    });
  };

  render() {
    const {hasTurn, canDrawCard} = this.state;
    // const deckDisabled = (isLoading || !hasTurn || hasDrawnCard);
    const deckDisabled = !canDrawCard;
    const playerCardsDisabled = (!hasTurn);

    return (
      <React.Fragment>
        <Hero size="fullheight" color="light" className="is-bold">
          <Hero.Body>
            <Container>

              <Columns className="game-columns" centered>
                <Columns.Column size={2} className="deck-column">
                  <Deck
                    cards={this.state.deck}
                    drawnCard={this.state.drawnCard}
                    disabled={deckDisabled}
                    handleDrawCard={this.handleDrawCard}
                    displayAlert={this.props.displayAlert} />
                </Columns.Column>
                
                <Columns.Column size={2} className="middle-column">
                  <div className="middle-wrap">
                    <div className="current-player-container">
                      <CurrentPlayer
                        currentPlayer={this.state.currentPlayer} />
                    </div>
                    <div className="pass-turn-container">
                      <PassTurnButton
                        canPassTurn={this.state.canPassTurn && this.state.hasTurn && !this.state.isLoading}
                        handlePassTurn={this.handlePassTurn} />
                    </div>
                  </div>
                </Columns.Column>
                
                <Columns.Column size={2} className="stack-column">
                  <Stack
                    cards={this.state.stack} />
                </Columns.Column>
              </Columns>

              <PlayerCards
                cards={this.state.cards}
                disabled={playerCardsDisabled}
                handlePlayCard={this.handlePlayCard}
                displayAlert={this.props.displayAlert} />

            </Container>
          </Hero.Body>
        </Hero>
        {this.state.showSuitChangeDialog &&
          <SuitChangeDialog
            onClose={this.closeSuitChangeDialog}
            card={this.state.suitChangeCard}
            onClick={this.handleClickSuitChangeCard} />}

        {this.state.showRankDemandDialog &&
          <RankDemandDialog
            onClose={this.closeRankDemandDialog}
            card={this.state.rankDemandCard}
            onClick={this.handleClickRankDemandCard} />}
      </React.Fragment>
    );
  }
}

export default TheGame;