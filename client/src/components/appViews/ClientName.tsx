import * as React from "react";
import {
  Box,
  Button,
  Columns,
  Container,
  Form,
  Heading,
  Hero
} from "react-bulma-components";

import Socket from "../../Socket";
import ResponseMessage from "../../class/ResponseMessage";
import AppViews from "../../class/enum/AppViews";
import ServerMessage from "../../class/enum/ServerMessage";
import ClientMessage from "../../class/enum/ClientMessage";

interface IProps {
  displayAlert: any,
  setClientNameAndSetView: any
}

interface IState {
  name: string,
  isLoading: boolean
}

class ClientName extends React.Component<IProps, IState> {
  state = {
    name: "",
    isLoading: false
  };

  componentDidMount() {
    Socket.on(ServerMessage.NameChangedSuccessfully, () => {
      this.props.setClientNameAndSetView(this.state.name, AppViews.RoomsList);
    });

    Socket.on(ServerMessage.NameIsAlreadyTaken, () => {
      this.setState({
        isLoading: false,
      }, () => {
        this.props.displayAlert(ResponseMessage.ClientNameIsAlreadyTaken);
      });
    });
  }

  componentWillUnmount() {
    Socket.off(ServerMessage.NameChangedSuccessfully);
    Socket.off(ServerMessage.NameIsAlreadyTaken);
  }

  handleInputKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.keyCode === 13) {
      this.handleButtonClick();
    }
  };

  handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      name: event.target.value
    });
  };

  validateClientName = (name: string): ResponseMessage => {
    if (name.length < 3) {
      return ResponseMessage.ClientNameTooShort;
    }
    
    // name contains special characters (including whitespaces)
    else if (/[ !@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(name)) {
      return ResponseMessage.ClientNameContainsSpecialCharacters;
    }
  
    return ResponseMessage.Success;
  };

  handleButtonClick = () => {
    const name = this.state.name;
    const response = this.validateClientName(name);
    if (response === ResponseMessage.Success) {
      this.setState({
        isLoading: true
      }, () => {
        Socket.emit(ClientMessage.Name, name)
      });
    } else {
      this.props.displayAlert(response);
    }
  };

  render() {
    return (
      <Hero size="fullheight" color="light" className="is-bold">
        <Hero.Body>
          <Container>
            <Columns centered>
              <Columns.Column size={4}>
                <Box textAlignment="centered">

                  <Heading>
                    What's your name?
                  </Heading>
                  
                  <Form.Field>
                    <Form.Control>
                      <Form.Input
                        size="medium"
                        placeholder="Your name..."
                        value={this.state.name}
                        onChange={this.handleInputChange}
                        onKeyDown={this.handleInputKeyDown} />
                    </Form.Control>
                  </Form.Field>

                  <Form.Field>
                    <Form.Control>
                      <Button
                        size="medium"
                        color="success"
                        fullwidth
                        onClick={this.handleButtonClick}
                        loading={this.state.isLoading}>
                        Explore servers
                      </Button>
                    </Form.Control>
                  </Form.Field>

                </Box>
              </Columns.Column>
            </Columns>
          </Container>
        </Hero.Body>
      </Hero>
    );
  }
}

export default ClientName;