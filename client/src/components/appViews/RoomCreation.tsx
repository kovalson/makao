import * as React from "react";
import {
  Form,
  Box,
  Heading,
  Button,
  Columns,
  Hero,
  Container,
  Icon
} from "react-bulma-components";

import AppViews from "../../class/enum/AppViews";
import ResponseMessage from "../../class/ResponseMessage";
import Socket from "../../Socket";
import CreatedRoomData from "../../class/CreatedRoomData";
import InvalidCreatedRoomData from "../../class/error/InvalidCreatedRoomData";
import ServerMessage from "../../class/enum/ServerMessage";
import ClientMessage from "../../class/enum/ClientMessage";

interface IProps {
  setAppView: any,
  displayAlert: any
}

interface IState {
  roomName: string,
  playersNumber: string,
  isLoading: boolean
}

class RoomCreation extends React.Component<IProps, IState> {
  state = {
    roomName: "",
    playersNumber: "2",
    isLoading: false
  };

  componentDidMount() {
    Socket.on(ServerMessage.RoomSuccessfullyCreated, (roomSocket: string) => {
      Socket.emit(ClientMessage.JoinRoom, roomSocket);
    });
    
    Socket.on(ServerMessage.SuccessfullyJoinedRoom, () => {
      this.props.setAppView(AppViews.TheRoom);
    });

    Socket.on(ServerMessage.RoomInputInvalid, () => {
      this.displayAlert(ResponseMessage.CreatedRoomDataInvalid);
    });

    Socket.on(ServerMessage.ClientAlreadyOwnsARoom, () => {
      this.displayAlert(ResponseMessage.ClientAlreadyOwnsARoom);
    });
  }

  componentWillUnmount() {
    Socket.off(ServerMessage.RoomSuccessfullyCreated);
    Socket.off(ServerMessage.SuccessfullyJoinedRoom);
    Socket.off(ServerMessage.RoomInputInvalid);
    Socket.off(ServerMessage.ClientAlreadyOwnsARoom);
  }

  displayAlert = (message: ResponseMessage) => {
    this.setState({
      isLoading: false
    }, () => {
      this.props.displayAlert(message);
    });
  };

  handleChangeRoomName = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      roomName: event.target.value
    });
  };

  handlePlayersNumber = (event: React.ChangeEvent<HTMLSelectElement>) => {
    this.setState({
      playersNumber: event.target.value
    });
  };

  createRoom = () => {
    const roomName = this.state.roomName.trim();
    const playersNumber = parseInt(this.state.playersNumber);

    try {
      const roomData = new CreatedRoomData(roomName, playersNumber);
      this.setState({
        isLoading: true
      }, () => {
        Socket.emit(ClientMessage.CreateRoom, roomData);
      });
    } catch(error) {
      if (error instanceof InvalidCreatedRoomData) {
        this.setState({
          isLoading: false
        }, () => {
          this.props.displayAlert(error.message);
        });
      }
    }
  };

  render() {
    return (
      <Hero size="fullheight" color="light" className="is-bold">
        <Hero.Body>
          <Container>
            <Columns centered>
              <Columns.Column size={4}>
                <Box>

                  <Heading>
                    Create room
                  </Heading>

                  <hr />

                  <Form.Field>
                    <Form.Label>Room name</Form.Label>
                    <Form.Control>
                      <Form.Input
                        size="medium"
                        type="text"
                        placeholder="Enter here..."
                        name="roomName"
                        onChange={this.handleChangeRoomName}
                        value={this.state.roomName}
                        disabled={this.state.isLoading} />
                    </Form.Control>
                  </Form.Field>

                  <Form.Field>
                    <Form.Label>Players number</Form.Label>
                    <Form.Control className="has-icons-left">
                      <Form.Select
                        name="playersNumber"
                        onChange={this.handlePlayersNumber}
                        value={this.state.playersNumber}
                        disabled={this.state.isLoading}>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </Form.Select>
                      <span className="icon is-left">
                        <i className="fas fa-user"></i>
                      </span>
                    </Form.Control>
                  </Form.Field>

                  <hr />

                  <Columns>
                    <Columns.Column className="is-half">
                      <Button
                        className="is-fullwidth is-medium is-success"
                        onClick={this.createRoom}
                        loading={this.state.isLoading}>
                        <Icon>
                          <i className="fas fa-check"></i>
                        </Icon>
                        <span>
                          Create
                        </span>
                      </Button>
                    </Columns.Column>
                    
                    <Columns.Column className="is-half">
                    <Button
                      className="is-fullwidth is-medium"
                      onClick={() => this.props.setAppView(AppViews.RoomsList)}
                      disabled={this.state.isLoading}>
                      <Icon>
                        <i className="fas fa-times"></i>
                      </Icon>
                      <span>
                        Cancel
                      </span>
                    </Button>
                    </Columns.Column>
                  </Columns>

                </Box>
              </Columns.Column>
            </Columns>
          </Container>
        </Hero.Body>
      </Hero>
    );
  }
}

export default RoomCreation;