import * as React from "react";
import {
  Hero,
  Container,
  Columns,
  Box,
  Heading,
  Form,
  Button
} from "react-bulma-components";

import RoomsTable from "../RoomsList/RoomsTable";
import NoRoomsFound from "../RoomsList/NoRoomsFound";
import AppViews from "../../class/enum/AppViews";
import Socket from "../../Socket";
import InvalidRoomDataError from "../../class/error/InvalidRoomDataError";
import ResponseMessage from "../../class/ResponseMessage";
import RoomsListElement from "../../class/RoomsListElement";
import InvalidRoomListElement from "../../class/error/InvalidRoomListElement";
import ServerMessage from "../../class/enum/ServerMessage";
import ClientMessage from "../../class/enum/ClientMessage";

interface IProps {
  setAppView: any,
  displayAlert: any
}

interface IState {
  rooms: Array<RoomsListElement>
}

class RoomsList extends React.Component<IProps, IState> {
  state = {
    rooms: []
  };

  componentDidMount() {
    Socket.on(ServerMessage.RoomsList, (roomsList: Array<object>) => {
      let rooms: Array<RoomsListElement> = [];
      roomsList.forEach((object) => {
        try {
          const room = RoomsListElement.fromObject(object);
          rooms.push(room);
        } catch(error) {
          if (error instanceof InvalidRoomListElement) {
            this.props.displayAlert(ResponseMessage.RoomListElementInvalid);
          }
        }
      });
      this.setState({
        rooms: rooms
      });
    });

    Socket.on(ServerMessage.SuccessfullyJoinedRoom, () => {
      this.props.setAppView(AppViews.TheRoom);
    });

    Socket.on(ServerMessage.RoomIsFull, () => {
      this.props.displayAlert(ResponseMessage.RoomIsFull);
    });

    Socket.on(ServerMessage.RoomUpdate, (roomObject: any) => {
      try {
        const newRoom = RoomsListElement.fromObject(roomObject);
        const rooms = this.state.rooms.map((room: RoomsListElement) => {
          if (room.getSocket() !== newRoom.getSocket()) {
            return room;
          } else {
            return newRoom;
          }
        });
        this.setState({
          rooms: rooms
        });
      } catch(error) {
        if (error instanceof InvalidRoomDataError) {
          this.props.displayAlert(ResponseMessage.RoomListElementInvalid);
        }
      }
    });
    
    Socket.on(ServerMessage.NewRoom, (roomObject: any) => {
      try {
        const room = RoomsListElement.fromObject(roomObject);
        const rooms = [...this.state.rooms, room];
        this.setState({
          rooms: rooms
        });
      } catch(error) {
        if (error instanceof InvalidRoomDataError) {
          this.props.displayAlert(ResponseMessage.RoomListElementInvalid);
        }
      }
    });
    
    Socket.on(ServerMessage.RoomDelete, (deletedSocket: string) => {
      const newRooms = this.state.rooms.filter((room: RoomsListElement) => {
        return (room.getSocket() !== deletedSocket);
      });
      this.setState({
        rooms: newRooms
      });
    });

    Socket.emit(ClientMessage.GetRooms);
  }

  componentWillUnmount() {
    Socket.off(ServerMessage.RoomsList);
    Socket.off(ServerMessage.SuccessfullyJoinedRoom);
    Socket.off(ServerMessage.RoomIsFull);
    Socket.off(ServerMessage.RoomUpdate);
    Socket.off(ServerMessage.NewRoom);
    Socket.off(ServerMessage.RoomDelete);
  }

  render() {
    const roomsNumber = this.state.rooms.length;

    return (
      <Hero size="fullheight" color="light" className="is-bold">
        <Hero.Body>
          <Container>
            <Columns centered>
              <Columns.Column size={7}>
                <Box>
                  
                  <Heading>
                    Rooms List
                  </Heading>

                  <hr />

                  {roomsNumber === 0 && <NoRoomsFound />}
                  {roomsNumber > 0 && <RoomsTable rooms={this.state.rooms} />}

                  <hr />

                  <Form.Field>
                    <Form.Control>
                      <Button
                        size="medium"
                        color="success"
                        onClick={() => this.props.setAppView(AppViews.RoomCreation)}>
                        Create new room
                      </Button>
                    </Form.Control>
                  </Form.Field>

                </Box>
              </Columns.Column>
            </Columns>
          </Container>
        </Hero.Body>
      </Hero>
    );
  }
}

export default RoomsList;