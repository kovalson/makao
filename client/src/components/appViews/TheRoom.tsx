import * as React from "react";
import {
  Hero,
  Container,
  Columns,
  Box,
  Heading,
  Button,
  Icon
} from "react-bulma-components";

import Socket from "../../Socket";
import AppViews from "../../class/enum/AppViews";
import PlayersTable from "../TheRoom/PlayersTable";
import RoomInfo from "../TheRoom/RoomInfo";
import RoomDetails from "../../class/RoomDetails";
import InvalidRoomDetails from "../../class/error/InvalidRoomDetails";
import ResponseMessage from "../../class/ResponseMessage";
import Player from "../../class/Player";
import InvalidPlayer from "../../class/error/InvalidPlayer";
import ServerMessage from "../../class/enum/ServerMessage";
import ClientMessage from "../../class/enum/ClientMessage";

interface IProps {
  setAppView: Function,
  displayAlert: Function,
  clientName: string
}

interface IState {
  room: RoomDetails,
  isClientReady: boolean,
  isLoading: boolean
}

class TheRoom extends React.Component<IProps, IState> {
  state = {
    room: RoomDetails.Empty(),
    isClientReady: false,
    isLoading: false
  };

  componentDidMount() {
    Socket.on(ServerMessage.RoomUpdate, (detailsObject: any) => {
      try {
        const roomDetails = RoomDetails.fromObject(detailsObject);
        this.setState({
          room: roomDetails
        });
      } catch(error) {
        if (error instanceof InvalidRoomDetails) {
          this.props.displayAlert(ResponseMessage.InvalidRoomDetails);
        }
      }
    });

    Socket.on(ServerMessage.RoomSuccessfullyLeft, () => {
      this.props.setAppView(AppViews.RoomsList);
    });

    Socket.on(ServerMessage.ClientIsReadyToPlay, (readyToPlay: boolean) => {
      this.setState({
        isLoading: false,
        isClientReady: readyToPlay
      });
    });

    Socket.on(ServerMessage.UpdateClientReadyToPlayStatus, (clientObject: any) => {
      this.setState({
        isLoading: false
      }, () => {
        try {
          const newPlayer: Player = Player.fromObject(clientObject);
          const newPlayers: Array<Player> = this.state.room.getPlayers().map((player: Player) => {
            if (player.getName() === newPlayer.getName()) {
              return newPlayer;
            } else {
              return player;
            }
          });
          const newRoom = this.state.room;
          newRoom.setPlayers(newPlayers);
          this.setState({
            room: newRoom
          });
        } catch(error) {
          if (error instanceof InvalidPlayer) {
            this.props.displayAlert(error.message)
          }
        }
      });
    });

    Socket.on(ServerMessage.GameStarted, () => {
      this.props.setAppView(AppViews.TheGame);
    });

    Socket.emit(ClientMessage.GetRoomInfo);
  }

  componentWillUnmount() {
    Socket.off(ServerMessage.RoomUpdate);
    Socket.off(ServerMessage.RoomSuccessfullyLeft);
    Socket.off(ServerMessage.ClientIsReadyToPlay);
    Socket.off(ServerMessage.UpdateClientReadyToPlayStatus);
    Socket.off(ServerMessage.GameStarted);
  }

  handleLeaveRoom = () => {
    Socket.emit(ClientMessage.LeaveRoom);
  };

  handleSetReady = () => {
    this.setState({
      isLoading: true
    }, () => {
      Socket.emit(ClientMessage.SetReady);
    });
  };
  
  render() {
    return (
      <Hero size="fullheight" color="light" className="is-bold">
        <Hero.Body>
          <Container>
            <Columns centered>
              <Columns.Column size={5}>
                <Box>
                  
                  <Heading marginless>
                    {this.state.room.getName()}
                  </Heading>

                  <hr />

                  <PlayersTable
                    room={this.state.room}
                    clientName={this.props.clientName} />

                  <RoomInfo
                    room={this.state.room} />

                  <hr />

                  <span className="buttons">
                    <Button
                      size="medium"
                      color="success"
                      loading={this.state.isLoading && !this.state.isClientReady}
                      disabled={this.state.isClientReady}
                      onClick={this.handleSetReady}>
                      {(this.state.isClientReady) ?
                        <div>
                          <Icon>
                            <i className="fas fa-check"></i>
                          </Icon>
                          <span>Ready to play</span>
                        </div>: "Ready to play"}
                    </Button>
                    
                    <Button
                      size="medium"
                      disabled={this.state.isLoading}
                      onClick={this.handleLeaveRoom}>
                      Leave room
                    </Button>
                  </span>

                </Box>
              </Columns.Column>
            </Columns>
          </Container>
        </Hero.Body>
      </Hero>
    );
  }
}

export default TheRoom;