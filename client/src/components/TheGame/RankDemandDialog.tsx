import * as React from "react";
import { Modal, Columns, Heading, Box } from "react-bulma-components";

import GameCard from "../../class/Card";
import "../../scss/suit-change-dialog.scss";
import Suit from "../../class/enum/Suit";

interface IProps {
  onClose: any,
  card: GameCard,
  onClick: Function
}

const RankDemandDialog: React.StatelessComponent<IProps> = ({ onClose, card, onClick }) => {
  return (
    <Modal show={true} onClose={onClose} closeOnBlur closeOnEsc>
      <Modal.Content className="suits-content">
        <Box>
          <Heading>Select rank</Heading>
          <Columns className="suits-select" centered multiline>
            <Columns.Column
              size="one-third"
              className="has-text-black"
              onClick={() => onClick(card, 5)}>
              5
            </Columns.Column>
            <Columns.Column
              size="one-third"
              className="has-text-black"
              onClick={() => onClick(card, 6)}>
              6
            </Columns.Column>
            <Columns.Column
              size="one-third"
              className="has-text-black"
              onClick={() => onClick(card, 7)}>
              7
            </Columns.Column>
            <Columns.Column
              size="one-third"
              className="has-text-black"
              onClick={() => onClick(card, 8)}>
              8
            </Columns.Column>
            <Columns.Column
              size="one-third"
              className="has-text-black"
              onClick={() => onClick(card, 9)}>
              9
            </Columns.Column>
            <Columns.Column
              size="one-third"
              className="has-text-black"
              onClick={() => onClick(card, 10)}>
              10
            </Columns.Column>
          </Columns>
        </Box>
      </Modal.Content>
    </Modal>
  );
};

export default RankDemandDialog;