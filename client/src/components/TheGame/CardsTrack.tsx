import * as React from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import GameCard from "../../class/Card";
import CardComponent from "./CardComponent";

interface IProps {
  cards: Array<GameCard>,
  handlePlayCard: Function,
  disabled: boolean
}

interface IState {
  cardsTrackOffset: number
}

class CardsTrack extends React.Component<IProps, IState> {
  private $cardsTrack: HTMLDivElement | null;
  private $innerDiv: HTMLDivElement | null;

  private lastX: number;
  private currentX: number;
  private hasCardsTrackDown: boolean;

  constructor(props: IProps) {
    super(props);
    
    this.$cardsTrack = null;
    this.$innerDiv = null;

    this.lastX = 0;
    this.currentX = 0;
    this.hasCardsTrackDown = false;
  }

  state = {
    cardsTrackOffset: 0
  }

  componentWillReceiveProps(nextProps: IProps) {
    if (nextProps.cards) {
      if (nextProps.cards.length > this.props.cards.length) {
        this.hasCardsTrackDown = false;
        this.setCameraAtLastCard();
      }
    }
  }

  setCameraAtLastCard = () => {
    const {$innerDiv, $cardsTrack} = this;
    
    if (!$innerDiv || !$cardsTrack) {
      return;
    }

    this.moveCardsTrack($innerDiv.clientWidth);
  };

  moveCardsTrack = (amount: number) => {
    const {$innerDiv, $cardsTrack} = this;
    if (!$innerDiv || !$cardsTrack) return;
    
    let nextOffset = this.state.cardsTrackOffset - amount;
    const diff = (nextOffset + $innerDiv.clientWidth) - ($cardsTrack.clientLeft + $cardsTrack.clientWidth);

    if ($innerDiv.clientWidth < $cardsTrack.clientWidth) {
      nextOffset = 0;
    } else {
      if (diff < 0) {
        nextOffset -= diff;
      } else if (nextOffset > $cardsTrack.clientLeft) {
        nextOffset -= Math.abs(nextOffset - $cardsTrack.clientLeft);
      }
    }

    this.setState({
      cardsTrackOffset: nextOffset
    });
  };

  onWheel = (event: React.WheelEvent<HTMLDivElement>) => {
    const sign: number = Math.sign(event.deltaY);
    this.moveCardsTrack(sign * 85);
  };

  onMouseMove = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const {clientX} = event;

    if (!this.hasCardsTrackDown) {
      return;
    }

    this.currentX = clientX;
    const dx = (this.currentX - this.lastX);
    this.lastX = clientX;
    
    this.moveCardsTrack(-dx);
  };

  onMouseDown = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    this.hasCardsTrackDown = true;
    this.currentX = event.clientX;
    this.lastX = event.clientX;
  };

  onMouseUp = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    this.hasCardsTrackDown = false;
    this.currentX = event.clientX;
    this.lastX = event.clientX;
  };

  renderCardComponent = (card: GameCard, index: number) => {
    if (this.props.disabled) {
      card.setDisabled(true);
    } else {
      card.setDisabled(false);
    }

    return (
      <CSSTransition
        key={card.getRank() + card.getSuit().toString()}
        in={true}
        appear={true}
        enter={true}
        timeout={300}
        classNames="card-component">
        <CardComponent
          onClick={this.props.handlePlayCard}
          card={card} />
      </CSSTransition>
    );
  };

  render() {
    const innerDivStyles = {
      left: this.state.cardsTrackOffset,
      transition: (this.hasCardsTrackDown) ? "" : "all 150ms"
    };

    return (
      <div
        className="cards-container"
        onWheel={(e) => this.onWheel(e)}
        onMouseDown={(e) => this.onMouseDown(e)}
        onMouseUp={(e) => this.onMouseUp(e)}
        onMouseMove={(e) => this.onMouseMove(e)}>

        <div
          className="cards-track"
          ref={(element) => this.$cardsTrack = element}>

          <div
            style={innerDivStyles}
            ref={(element) => this.$innerDiv = element}>
            
            <TransitionGroup>
              {this.props.cards.sort((a: GameCard, b: GameCard) => {
                // return (a.getRank() - b.getRank());
                if (a.getRank() > b.getRank()) {
                  return 1;
                } else if (a.getRank() < b.getRank()) {
                  return -1;
                } else {
                  return a.getSuit().toString().localeCompare(b.getSuit().toString());
                }
              }).map(this.renderCardComponent)}
            </TransitionGroup>
          </div>
        </div>
      </div>
    );
  }
}

export default CardsTrack;