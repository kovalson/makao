import * as React from "react";
import { Heading } from "react-bulma-components";
import { TransitionGroup, CSSTransition } from "react-transition-group";

import "../../scss/react-transition-group/current-player.scss";

interface IProps {
  currentPlayer: string
}

const CurrentPlayer: React.StatelessComponent<IProps> = ({ currentPlayer }) => {
  return (
    <div className="player-turn-wrap">
      <Heading size={6}>
        {(currentPlayer) ? "CURRENT PLAYER" : ""}
      </Heading>
      <TransitionGroup>
        <CSSTransition
          key={currentPlayer}
          timeout={300}
          in={true}
          appear={true}
          enter={true}
          exit={true}
          classNames="player-turn">
          <div className="player-turn-container">
            <div>
              <Heading size={4}>
                {currentPlayer}
              </Heading>
            </div>
          </div>
        </CSSTransition>
      </TransitionGroup>
    </div>
  );
}

export default CurrentPlayer;