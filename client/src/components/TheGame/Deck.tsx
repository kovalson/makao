import * as React from "react";
import { Card, Heading } from "react-bulma-components";
import { TransitionGroup, CSSTransition } from "react-transition-group";

import Suit from "../../class/enum/Suit";
import GameCard from "../../class/Card";
import "../../scss/deck.scss";

interface IProps {
  cards: Array<GameCard>,
  drawnCard: GameCard,
  disabled: boolean,
  handleDrawCard: Function,
  displayAlert: Function
}

interface IState {
  cards: Array<GameCard>,
  cardsNumber: number
}

class Deck extends React.Component<IProps, IState> {
  private clickedElement: HTMLDivElement | null;
  private flipTimeout: number;

  constructor(props: IProps) {
    super(props);
    this.clickedElement = null;
    this.flipTimeout = 0;
  }

  state = {
    cards: this.props.cards,
    cardsNumber: 5
  }

  componentWillUnmount() {
    clearTimeout(this.flipTimeout);
  }

  componentWillReceiveProps(nextProps: IProps) {
    if (nextProps.cards.length !== this.props.cards.length) {
      this.setState({
        cards: nextProps.cards
      });
    } else if (nextProps.drawnCard) {
      const newCards = this.state.cards;
      newCards[newCards.length - 1] = nextProps.drawnCard;
      this.setState({
        cards: newCards
      }, () => {
        this.flipTimeout = window.setTimeout(() => {
          if (this.clickedElement) {
            this.clickedElement.classList.add("flipped");
          }
        }, 50);
      });
    }
  }

  handleClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (!this.props.disabled) {
      this.clickedElement = event.currentTarget;
      this.props.handleDrawCard();
    }
  };

  renderCard = (card: GameCard, index: number) => {
    const cardStyles = {
      position: "absolute",
      top: index / 4
    };
    const backStyles = {
      position: "absolute",
      top: index / 4
    };
    const suit = card.getSuit().toString();
    const color = (suit === Suit.Clubs || suit === Suit.Spades) ? "black" : "danger";
    const classDisabled = (this.props.disabled) ? " disabled" : "";

    return (
      <CSSTransition
        key={index}
        in={true}
        appear={true}
        enter={true}
        exit={true}
        timeout={200}
        classNames="deck-card">
        <div className={"flip-card" + classDisabled} onClick={(e) => this.handleClick(e)}>
          <div className="flip-card-inner">
            <div className="front">
              <Card
                className="game-card"
                style={cardStyles}>
                <Card.Header>
                  ?
                </Card.Header>
                <Card.Content textAlignment="centered">
                  <Heading className="deck-card-content">
                    ?
                  </Heading>
                </Card.Content>
                <Card.Footer>
                  ?
                </Card.Footer>
              </Card>
            </div>
            <div className="back">
              <Card
                className="game-card"
                style={backStyles}>
                <Card.Header className={"has-text-" + color}>
                  {card.getTextRank()}
                </Card.Header>
                <Card.Content textAlignment="centered">
                  <Heading className={"deck-card-content has-text-" + color}>
                    {(suit === Suit.Clubs) && "♣"}
                    {(suit === Suit.Hearts) && "♥"}
                    {(suit === Suit.Diamonds) && "♦"}
                    {(suit === Suit.Spades) && "♠"}
                    {(suit === Suit.None) && "?"}
                  </Heading>
                </Card.Content>
                <Card.Footer className={"has-text-" + color}>
                  {card.getTextRank()}
                </Card.Footer>
              </Card>
            </div>
          </div>
        </div>
      </CSSTransition>
    );
  };

  render() {
    return (
      <div className="deck-wrap">
        <Heading>
          Deck
        </Heading>
        <TransitionGroup className="deck">
          {this.state.cards.map(this.renderCard)}
        </TransitionGroup>
      </div>
    );
  }
}

export default Deck;