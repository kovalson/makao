import * as React from "react";
import { Card, Heading } from "react-bulma-components";

import GameCard from "../../class/Card";
import Suit from "../../class/enum/Suit";

interface IProps {
  card: GameCard,
  onClick: Function
}

const CardComponent: React.StatelessComponent<IProps> = ({ card, onClick }) => {
  const rank = card.getTextRank();
  const suit = card.getSuit();
  const color = (suit === Suit.Clubs || suit === Suit.Spades) ? "black" : "danger";
  const disabled = card.isDisabled();
  const unplayable = !card.isPlayable();
  
  return (
    <div
      className={"card-wrap" + ((disabled || unplayable) ? " disabled" : "")}
      onClick={() => onClick(card)}>
      <Card className="game-card">
        <Card.Header className={"has-text-" + color}>
          {rank}
        </Card.Header>
        <Card.Content textAlignment="centered">
          <Heading className={"has-text-"+ color}>
            {(suit === Suit.Clubs) && "♣"}
            {(suit === Suit.Hearts) && "♥"}
            {(suit === Suit.Diamonds) && "♦"}
            {(suit === Suit.Spades) && "♠"}
          </Heading>
        </Card.Content>
        <Card.Footer className={"has-text-" + color}>
          {rank}
        </Card.Footer>
      </Card>
    </div>
  );
};

export default CardComponent;