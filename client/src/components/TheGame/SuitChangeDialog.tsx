import * as React from "react";
import { Modal, Columns, Heading, Container, Box } from "react-bulma-components";

import GameCard from "../../class/Card";
import "../../scss/suit-change-dialog.scss";
import Suit from "../../class/enum/Suit";

interface IProps {
  onClose: any,
  card: GameCard,
  onClick: Function
}

const SuitChangeDialog: React.StatelessComponent<IProps> = ({ onClose, card, onClick }) => {
  return (
    <Modal show={true} onClose={onClose} closeOnBlur closeOnEsc>
      <Modal.Content className="suits-content">
        <Box>
          <Heading>Select suit</Heading>
          <Columns className="suits-select" centered>
            <Columns.Column
              className="has-text-danger"
              onClick={() => onClick(card, Suit.Diamonds)}>
              ♦
            </Columns.Column>
            <Columns.Column
              className="has-text-black"
              onClick={() => onClick(card, Suit.Clubs)}>
              ♣
            </Columns.Column>
            <Columns.Column
              className="has-text-danger"
              onClick={() => onClick(card, Suit.Hearts)}>
              ♥
            </Columns.Column>
            <Columns.Column
              className="has-text-black"
              onClick={() => onClick(card, Suit.Spades)}>
              ♠
            </Columns.Column>
          </Columns>
        </Box>
      </Modal.Content>
    </Modal>
  );
};

export default SuitChangeDialog;