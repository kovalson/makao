import * as React from "react";

import GameCard from "../../class/Card";
import CardsTrack from "./CardsTrack";
import "./../../scss/player-cards.scss";

interface IProps {
  disabled: boolean,
  cards: Array<GameCard>,
  handlePlayCard: Function,
  displayAlert: Function
}

class PlayerCards extends React.Component<IProps, {}> {
  render() {
    return (
      <CardsTrack
        cards={this.props.cards}
        handlePlayCard={this.props.handlePlayCard}
        disabled={this.props.disabled} />
    );
  }
}

export default PlayerCards;