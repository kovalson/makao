import * as React from "react";
import { Card, Heading } from "react-bulma-components";

import GameCard from "./../../class/Card";
import Suit from "../../class/enum/Suit";
import "../../scss/stack.scss";
import { TransitionGroup, CSSTransition } from "react-transition-group";

interface IProps {
  cards: Array<GameCard>
}

class Stack extends React.Component<IProps, {}> {
  renderCard = (card: GameCard, index: number) => {
    const rank = (card.getRank()) ? card.getTextRank() : "?";
    const suit = card.getSuit();
    const rotation = card.getStackRotation();
    const color = (suit === Suit.Diamonds || suit === Suit.Hearts) ? "danger" : "black";
    const cardStyles = {
      position: "absolute",
      transform: `rotate(${rotation}deg)`
    };
    const suitVisible = (card.isSuitVisible());
    const suitClassName = "card-suit" + ((suitVisible) ? "" : " hidden") + " has-text-" + color;
    const rankDemand = card.getRankDemand();

    return (
      <CSSTransition
        key={index}
        in={true}
        appear={true}
        enter={true}
        timeout={200}
        classNames="stack-card">
        <Card
          className="game-card"
          style={cardStyles}>
          <Card.Header className={"card-rank-header has-text-" + color}>
            {rank}
          </Card.Header>
          <Card.Content textAlignment="centered">
            <Heading className={suitClassName}>
              {(suit === Suit.Clubs) && "♣"}
              {(suit === Suit.Hearts) && "♥"}
              {(suit === Suit.Diamonds) && "♦"}
              {(suit === Suit.Spades) && "♠"}
              {(suit === Suit.None) && "?"}
              {(suit.constructor === Number) && suit}
            </Heading>
          </Card.Content>
          <Card.Footer className={"card-rank-footer has-text-" + color}>
            {rank}
          </Card.Footer>
        </Card>
      </CSSTransition>
    );
  };

  render() {
    return (
      <div className="stack-wrap">
        <Heading>
          Stack
        </Heading>
        <TransitionGroup className="stack">
          {this.props.cards.map(this.renderCard)}
        </TransitionGroup>
      </div>
    );
  }
}

export default Stack;