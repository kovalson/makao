import * as React from "react";
import { Button } from "react-bulma-components";

import "./../../scss/pass-turn-button.scss";

interface IProps {
  canPassTurn: boolean,
  handlePassTurn: Function
}

const PassTurnButton: React.StatelessComponent<IProps> = ({ canPassTurn, handlePassTurn }) => {
  const show = (canPassTurn) ? " show" : "";
  const desktopClassName = "pass-turn-button" + show;
  const mobileClassName = "pass-turn-button-mobile" + show;
  const isDisabled = (!canPassTurn);

  return (
    <React.Fragment>
      <Button
        size="medium"
        color="info"
        className={desktopClassName}
        disabled={isDisabled}
        responsive={{ mobile: { hide: { value: true } } }}
        onClick={() => handlePassTurn()}>
        End Turn
      </Button>
      <Button
        size="medium"
        color="info"
        className={mobileClassName}
        disabled={isDisabled}
        responsive={{ tablet: { hide: { value: true } } }}
        onClick={() => handlePassTurn()}>
        End Turn
      </Button>
    </React.Fragment>
  );
};

export default PassTurnButton;