import * as React from "react";
import {
  Modal,
  Message
} from "react-bulma-components";

import ResponseMessage from "./../class/ResponseMessage";

interface IProps {
  message: ResponseMessage | string
  onClose: any
}

const Alert: React.StatelessComponent<IProps> = (props) => {
  const {message, onClose} = props;

  return (
    <Modal show={true} onClose={onClose} closeOnBlur closeOnEsc>
      <Modal.Content>
        <Message color="danger">
          <Message.Header textAlignment="centered">
            Error!
          </Message.Header>
          <Message.Body textAlignment="centered">
            <p>
              {message}
            </p>
          </Message.Body>
        </Message>
      </Modal.Content>
    </Modal>
  );
};

export default Alert;