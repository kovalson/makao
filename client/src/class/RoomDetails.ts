import RoomsListElement from "./RoomsListElement";
import InvalidRoomDetails from "./error/InvalidRoomDetails";
import Player from "./Player";
import InvalidPlayer from "./error/InvalidPlayer";

class RoomDetails extends RoomsListElement {
  public static fromObject(object: any): RoomDetails {
    const {name, playersNumber, currentPlayersNumber} = object;
    const playersArray = object.players;
    const ownerObject = object.owner;
    const stateObject = object.state;

    if (typeof name === "undefined")
      throw InvalidRoomDetails.InvalidName();
    if (typeof playersNumber === "undefined")
      throw InvalidRoomDetails.InvalidPlayersNumber();
    if (typeof currentPlayersNumber === "undefined")
      throw InvalidRoomDetails.InvalidCurrentPlayersNumber();
    if (typeof playersArray === "undefined")
      throw InvalidRoomDetails.InvalidPlayersArray();
    if (typeof ownerObject === "undefined")
      throw InvalidRoomDetails.InvalidOwner();
    if (typeof stateObject === "undefined")
      throw InvalidRoomDetails.InvalidState();
    
    const owner = ownerObject.name;
    const roomSocket = ownerObject.socket;
    if (!owner || !roomSocket)
      throw InvalidRoomDetails.InvalidOwner();

    const state = stateObject.name;
    if (!state)
      throw InvalidRoomDetails.InvalidState();
    
    const players: Array<Player> = [];
    playersArray.forEach((object: any) => {
      try {
        const player = Player.fromObject(object);
        players.push(player);
      } catch(error) {
        if (error instanceof InvalidPlayer) {
          players.push(Player.Invalid());
        }
      }
    });

    return new RoomDetails(
      name,
      playersNumber,
      currentPlayersNumber,
      players,
      owner,
      roomSocket,
      state);
  }

  public static Empty(): RoomDetails {
    return new RoomDetails("", 0, 0, [], "", "", "");
  }

  private players: Array<Player>;

  constructor(name: string, playersNumber: number, currentPlayersNumber: number, playersArray: Array<Player>, owner: string, socket: string, state: string) {
    super(name, playersNumber, currentPlayersNumber, owner, socket, state);
    this.players = playersArray;
  }

  public getPlayers(): Array<Player> {
    return this.players;
  }

  public setPlayers(players: Array<Player>): void {
    this.players = players;
  }
}

export default RoomDetails;