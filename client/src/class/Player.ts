import InvalidPlayer from "./error/InvalidPlayer";

class Player {
  public static fromObject(object: any): Player {
    const {name, control, readyToPlay} = object;

    if (typeof name === "undefined")
      throw InvalidPlayer.InvalidName();
    if (typeof control === "undefined")
      throw InvalidPlayer.InvalidControl();
    if (typeof readyToPlay === "undefined")
      throw InvalidPlayer.InvalidReadyToPlay();

    return new Player(name, control, readyToPlay);
  }

  public static Invalid(): Player {
    return new Player("Invalid Player!", "ERROR");
  }

  private name: string;
  private control: string;
  private readyToPlay: boolean;

  constructor(name: string, control: string, readyToPlay?: boolean) {
    this.name = name;
    this.control = control;
    this.readyToPlay = readyToPlay || false;
  }

  public getName(): string {
    return this.name;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public getControl(): string {
    return this.control;
  }

  public setControl(control: string): void {
    this.control = control;
  }

  public isReadyToPlay(): boolean {
    return this.readyToPlay;
  }

  public setReadyToPlay(bool: boolean): void {
    this.readyToPlay = bool;
  }
}

export default Player;