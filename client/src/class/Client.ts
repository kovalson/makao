import InvalidOwnerError from "./error/InvalidOwnerError";
import Player from "./Player";

class Client extends Player {
  public static Empty() {
    return new Client("", "");
  }
  
  public static fromObject(object: any): Client {
    if (
      typeof object.name === "undefined" ||
      typeof object.socket === "undefined"
    ) {
      throw new InvalidOwnerError();
    }
    
    return new Client(
      object.name,
      object.socket
    );
  }

  private socket: string;
  
  constructor(name: string, socket: string) {
    super(name, Client.name);
    this.socket = socket;
  }

  public getSocket() {
    return this.socket;
  }
}

export default Client;