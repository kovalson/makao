import InvalidRoomStateError from "./error/InvalidRoomStateError";

class RoomState {
  public static Empty() {
    return new RoomState("");
  }

  public static fromObject(object: any): RoomState {
    if (typeof object.name === "undefined") {
      throw new InvalidRoomStateError();
    }
    return new RoomState(object.name);
  }

  private name: string;

  constructor(name: string) {
    this.name = name;
  }

  public getName() {
    return this.name;
  }
}

export default RoomState;