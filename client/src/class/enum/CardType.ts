enum CardType {
  Regular = "regular",
  Combat = "combat",
  Skip = "skip",
  RankDemanding = "rank demanding",
  SuitChanging = "suit changing"
}

export default CardType;