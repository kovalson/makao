enum Suit {
  None = "None",
  
  Spades = "Spades",
  Hearts = "Hearts",
  Diamonds = "Diamonds",
  Clubs = "Clubs"
}

export default Suit;