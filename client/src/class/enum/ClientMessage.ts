enum ClientMessage {
  Connection = "connection",
  Disconnect = "disconnect",
  
  Name = "client name",

  CreateRoom = "create room",
  GetRooms = "get rooms",
  JoinRoom = "join room",
  LeaveRoom = "leave room",
  GetRoomInfo = "get room info",

  SetReady = "set ready",
  AddBot = "add bot",
  RemoveFromRoom = "remove from room",

  GetCards = "get cards",
  PlayCard = "play card",
  DrawCard = "draw card",
  PassTurn = "pass turn"
}

export default ClientMessage;