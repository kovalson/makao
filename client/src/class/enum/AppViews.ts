enum AppViews {
  ConnectingToServer,
  ConnectionLost,
  ClientName,
  RoomsList,
  RoomCreation,
  TheRoom,
  TheGame
}

export default AppViews;