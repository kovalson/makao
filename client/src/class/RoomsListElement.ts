import InvalidRoomListElement from "./error/InvalidRoomListElement";

class RoomsListElement {
  public static fromObject(object: any): RoomsListElement {
    const name = object.name;
    const playersNumber = object.playersNumber;
    const currentPlayersNumber = object.currentPlayersNumber;
    const ownerObject = object.owner;
    const stateObject = object.state;

    if (typeof name === "undefined")
      throw InvalidRoomListElement.InvalidName();
    if (typeof playersNumber === "undefined")
      throw InvalidRoomListElement.InvalidPlayersNumber();
    if (typeof currentPlayersNumber === "undefined")
      throw InvalidRoomListElement.InvalidCurrentPlayersNumber();
    if (typeof ownerObject === "undefined")
      throw InvalidRoomListElement.InvalidOwner();
    if (typeof stateObject === "undefined")
      throw InvalidRoomListElement.InvalidState();
    
    const owner = ownerObject.name;
    const roomSocket = ownerObject.socket;
    if (!owner || !roomSocket)
      throw InvalidRoomListElement.InvalidOwner();

    const state = stateObject.name;
    if (!state)
      throw InvalidRoomListElement.InvalidState();

    return new RoomsListElement(
      name,
      playersNumber,
      currentPlayersNumber,
      owner,
      roomSocket,
      state
    );
  }

  private name: string;
  private playersNumber: number;
  private currentPlayersNumber: number;
  private owner: string;
  private socket: string;
  private state: string;

  constructor(name: string, players: number, currentPlayers: number, owner: string, socket: string, state: string) {
    this.name = name;
    this.playersNumber = players;
    this.currentPlayersNumber = currentPlayers;
    this.owner = owner;
    this.socket = socket;
    this.state = state;
  }

  public getName(): string {
    return this.name;
  }

  public getPlayersNumber(): number {
    return this.playersNumber;
  }

  public getCurrentPlayersNumber(): number {
    return this.currentPlayersNumber;
  }

  public getOwner(): string {
    return this.owner;
  }

  public getSocket(): string {
    return this.socket;
  }

  public getState(): string {
    return this.state;
  }
}

export default RoomsListElement;