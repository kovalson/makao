enum ResponseMessage {
  Success = "Success",
  InvalidInput = "Given input is invalid!",
  ClientNameIsAlreadyTaken = "This name is already taken!",
  ClientNameContainsSpecialCharacters = "Name cannot contain any special characters (including whitespaces)!",
  ClientNameTooShort = "Client name is too short!",
  RoomNameContainsSpecialCharacters = "Room name cannot contain special characters!",
  RoomNameTooShort = "Room name is too short!",
  ClientAlreadyOwnsARoom = "You already own a room!",
  RoomIsFull = "Room is full!",
  RoomListElementInvalid = "Room list element is invalid!",
  CreatedRoomDataInvalid = "Created room data is invalid!",
  InvalidRoomDetails = "Invalid room details object!",
  NotYourTurn = "It is not your turn!"
}

export default ResponseMessage;