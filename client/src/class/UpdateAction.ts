import ServerMessage from "./enum/ServerMessage";

export default interface UpdateAction {
  type: ServerMessage,
  data: any
}