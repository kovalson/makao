import Suit from "./enum/Suit";
import CardType from "./enum/CardType";

class Card {
  public static Empty(): Card {
    return new Card(0, Suit.None, CardType.Regular, false, Suit.None, 0);
  }

  public static fromObject(object: any): Card {
    const { rank, suit, playable, type, changedSuit, rankDemand } = object;

    let suitObject: Suit = Suit.Clubs;
    if (suit === "Hearts") suitObject = Suit.Hearts;
    if (suit === "Spades") suitObject = Suit.Spades;
    if (suit === "Diamonds") suitObject = Suit.Diamonds;

    let changedSuitObject: Suit = Suit.Clubs;
    if (changedSuit === "Hearts") changedSuitObject = Suit.Hearts;
    if (changedSuit === "Spades") changedSuitObject = Suit.Spades;
    if (changedSuit === "Diamonds") changedSuitObject = Suit.Diamonds;

    let typeObject: CardType = CardType.Regular;
    if (type === "combat") typeObject = CardType.Combat;
    if (type === "skip") typeObject = CardType.Skip;
    if (type === "rank demanding") typeObject = CardType.RankDemanding;
    if (type === "suit changing") typeObject = CardType.SuitChanging;

    return new Card(rank, suitObject, typeObject, playable, changedSuitObject, rankDemand);
  }

  private rank: number;
  private suit: Suit | number;
  private type: CardType;
  private playable: boolean;
  private disabled: boolean;
  private changedSuit: Suit;
  private rankDemand: number;

  private stackRotation: number;
  private deckRotation: number;
  private suitVisible: boolean;
  
  constructor(rank: number, suit: Suit, type: CardType, playable: boolean, changedSuit: Suit, rankDemand: number) {
    this.rank = rank;
    this.suit = suit;
    this.type = type;
    this.playable = playable;
    this.changedSuit = changedSuit;
    this.rankDemand = rankDemand;
    this.disabled = false;
    this.suitVisible = true;

    this.stackRotation = (Math.random() * 20 | 0) * ((Math.random() >= 0.5) ? 1 : -1);
    this.deckRotation = (Math.random() * 10 | 0) * ((Math.random() >= 0.5) ? 1 : -1);
  }

  public getRank(): number {
    return this.rank;
  }

  public getSuit(): Suit | number {
    return this.suit;
  }

  public isDisabled(): boolean {
    return this.disabled;
  }

  public setDisabled(bool: boolean): void {
    this.disabled = bool;
  }
  
  public isPlayable(): boolean {
    return this.playable;
  }

  public setPlayable(bool: boolean): void {
    this.playable = bool;
  }

  public getStackRotation(): number {
    return this.stackRotation;
  }

  public getDeckRotation(): number {
    return this.deckRotation;
  }

  public getType(): CardType {
    return this.type;
  }

  public getChangedSuit(): Suit {
    return this.changedSuit;
  }

  public setChangedSuit(suit: Suit): void {
    this.changedSuit = suit;
  }

  public applyChangedSuit(): void {
    this.suit = this.changedSuit;
  }

  public applyRankDemand(): void {
    this.suit = this.rankDemand;
  }
  
  public isSuitVisible(): boolean {
    return this.suitVisible;
  }

  public hideSuit(): void {
    this.suitVisible = false;
  }

  public showSuit(): void {
    this.suitVisible = true;
  }

  public getRankDemand(): number {
    return this.rankDemand;
  }

  public setRankDemand(rank: number): void {
    this.rankDemand = rank;
  }

  public isType(type: CardType): boolean {
    return this.type === type;
  }

  public setHiddenValues(rank: number, suit: string): void {
    this.rank = rank;
    this.suit = Suit.Clubs;
    if (suit === "Hearts") this.suit = Suit.Hearts;
    if (suit === "Spades") this.suit = Suit.Spades;
    if (suit === "Diamonds") this.suit = Suit.Diamonds;
  }

  public getTextRank(): string {
    if (this.rank === 11)
      return "J";
    else if (this.rank === 12)
      return "Q";
    else if (this.rank === 13)
      return "K";
    else if (this.rank === 14)
      return "A";
    return this.rank.toString();
  }

  public getSendable(): object {
    return {
      rank: this.rank,
      suit: this.suit.toString(),
      changedSuit: this.changedSuit,
      rankDemand: this.rankDemand
    };
  }
}

export default Card;