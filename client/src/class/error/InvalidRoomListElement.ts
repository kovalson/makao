class InvalidRoomListElement extends Error {
  public static InvalidName(): InvalidRoomListElement {
    return new InvalidRoomListElement("Room name is invalid!");
  }
  
  public static InvalidPlayersNumber(): InvalidRoomListElement {
    return new InvalidRoomListElement("Players number is invalid!");
  }
  
  public static InvalidCurrentPlayersNumber(): InvalidRoomListElement {
    return new InvalidRoomListElement("Current players number is invalid!");
  }

  public static InvalidOwner(): InvalidRoomListElement {
    return new InvalidRoomListElement("Room owner is invalid!");
  }

  public static InvalidState(): InvalidRoomListElement {
    return new InvalidRoomListElement("Room state is invalid!");
  }
}

export default InvalidRoomListElement;