class InvalidPlayer extends Error {
  public static InvalidName() {
    return new InvalidPlayer("Player's name is invalid!");
  }

  public static InvalidControl() {
    return new InvalidPlayer("Player's control is invalid!");
  }

  public static InvalidReadyToPlay() {
    return new InvalidPlayer("Player's readyToPlay is invalid!");
  }
}

export default InvalidPlayer;