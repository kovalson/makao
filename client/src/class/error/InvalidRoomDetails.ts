import InvalidRoomListElement from "./InvalidRoomListElement";

class InvalidRoomDetails extends InvalidRoomListElement {
  static InvalidPlayersArray() {
    return new InvalidRoomDetails("Players array is invalid!");
  }
}

export default InvalidRoomDetails;