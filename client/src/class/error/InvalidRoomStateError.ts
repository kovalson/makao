class InvalidRoomStateError extends Error {}

export default InvalidRoomStateError;