class InvalidCreatedRoomData extends Error {
  public static NameTooShort(): InvalidCreatedRoomData {
    return new InvalidCreatedRoomData("Room name is too short!");
  }

  public static SpecialCharacters(): InvalidCreatedRoomData {
    return new InvalidCreatedRoomData("Room name cannot contain special characters!");
  }

  public static InvalidName(): InvalidCreatedRoomData {
    return new InvalidCreatedRoomData("Invalid room name!");
  }

  public static InvalidPlayersNumber(): InvalidCreatedRoomData {
    return new InvalidCreatedRoomData("Invalid players number!");
  }

  public static InsufficientPlayersNumber(): InvalidCreatedRoomData {
    return new InvalidCreatedRoomData("Insufficient players number!");
  }
}

export default InvalidCreatedRoomData;