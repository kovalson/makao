class InvalidCard extends Error {
  public static RankUndefined(): InvalidCard {
    return new InvalidCard("Rank is undefined!");
  }

  public static SuitUndefined(): InvalidCard {
    return new InvalidCard("Suit is undefined!");
  }

  public static RankNotNumber(rank: any): InvalidCard {
    return new InvalidCard("Rank " + rank + " must be a number!");
  }

  public static InvalidRank(rank: number): InvalidCard {
    return new InvalidCard("Rank " + rank + " is invaild!");
  }
}

export default InvalidCard;