import InvalidCreatedRoomData from "./error/InvalidCreatedRoomData";

class CreatedRoomData {
  private name: string;
  private playersNumber: number;

  constructor(name: string, playersNumber: number) {
    if (name.length < 3) {
      throw InvalidCreatedRoomData.NameTooShort();
    } else if (/[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(name)) {
      throw InvalidCreatedRoomData.SpecialCharacters();
    } else if (playersNumber < 2) {
      throw InvalidCreatedRoomData.InsufficientPlayersNumber();
    }

    this.name = name;
    this.playersNumber = playersNumber;
  }

  public getSendable(): object {
    return {
      name: this.name,
      playersNumber: this.playersNumber
    };
  }
}

export default CreatedRoomData;