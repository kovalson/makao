import { Room } from "./Room";
import { Client } from "./Client";
import { ResponseMessage } from "./ResponseMessage";
import { Game } from "./Game";

export abstract class RoomState {
  private room: Room;
  
  constructor(room: Room) {
    this.room = room;
  }

  public addClient(client: Client): ResponseMessage {
    return ResponseMessage.Success();
  }

  public removeClient(socketId: string): void {
    // do nothing
  }

  /**
   * Updates all private variables of room state.
   * Returns true always if variables can still be changed
   * during future updates. If variables meet conditions
   * required for the state to change this method returns false.
   */
  public update(): boolean {
    return true;
  }

  public getRoom(): Room {
    return this.room;
  }

  public getGame(): Game {
    return null;
  }

  public getSendable(): object {
    return {
      name: this.constructor.name
    };
  }
}