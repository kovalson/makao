import { GameState } from "../GameState";
import { Player } from "../Player";
import { Client } from "../Client";
import { Card } from "../Card";
import CardType from "../enum/CardType";
import { Game } from "../Game";
import { GS_Regular } from "./GS_Regular";

class GS_Skip extends GameState {
  private lastPlayer: Player;
  private turnsToSkip: number;

  constructor(game: Game, lastPlayer: Player) {
    super(game);
    this.lastPlayer = lastPlayer;
    this.turnsToSkip = 0;
  }

  public removeClient(socketId: string): void {
    const room = this.getGame().getRoom();
    const newPlayers = room.getPlayers().filter((player: Player) => {
      if (player instanceof Client) {
        return (player.getSocket() !== socketId);
      } else {
        return true;
      }
    });
    room.setPlayers(newPlayers);

    // TODO: handle checking-up flags such as hasTurn, etc.
  }

  public isCardPlayable(card: Card): boolean {
    return (card.isType(CardType.Skip));
  }

  public playCard(card: Card): void {
    const game = this.getGame();
    const newStack = this.getGame().getStack();
    newStack.push(card);
    game.setStack(newStack);
    this.turnsToSkip++;
  }

  public canPlayerDrawCard(player: Player): boolean {
    return false;
  }

  public canPlayerPassTurn(player: Player): boolean {
    return true;
  }

  /*
    Idea:
      - if current player passed turn:
        - the state changes to regular
        - current player skips next {this.turnsToSkip} turns
      - if current player played a card:
        - the state continues with incremented {this.turnsToSkip} value
      - if current player is already skipping some turns:
        - the state continues with the same value
      - if next player is the last player ({this.lastPlayer}) who
        played a skip-card:
        - the state changes to regular (as player cannot play a skip-card
          on himself)
  */
  public passTurn(): void {
    const game = this.getGame();
    const room = game.getRoom();
    const players = room.getPlayers();
    const currentPlayerObject = game.getCurrentPlayer();
    const playerPassingTurn = currentPlayerObject.object;
    let nextIndex = (currentPlayerObject.index + 1) % players.length;

    while (players[nextIndex].isWinner()) {
      nextIndex = (nextIndex + 1) % players.length;
    }

    const nextPlayer = players[nextIndex];

    game.setCurrentPlayer({
      index: nextIndex,
      object: nextPlayer
    });

    if (playerPassingTurn.hasPlayedCard()) {
      this.turnsToSkip++;
      this.lastPlayer = playerPassingTurn;
      game.setNextState(this);
      console.log("Skip: state remains (has played card)");
    } else {
      if (playerPassingTurn.skipsTurn()) {
        if (nextPlayer.getName() === this.lastPlayer.getName()) {
          game.setNextState(new GS_Regular(game));
          console.log("Skip: state changed to Regular (last === next)");
        } else {
          game.setNextState(this);
          console.log("Skip: state remains (skips turn)");
        }
      } else {
        playerPassingTurn.setTurnsToSkip(this.turnsToSkip);
        game.setNextState(new GS_Regular(game));
        console.log("Skip: state changed to Regular (passed turn)");
      }
    }

    playerPassingTurn.setTurnsToSkip(Math.max(playerPassingTurn.getTurnsToSkip() - 1, 0));
    playerPassingTurn.setDrawnCard(false);
    playerPassingTurn.setPlayedCard(false);
    playerPassingTurn.setHasTurn(false);
  }
}

export default GS_Skip;