import { GameState } from "../GameState";
import { Game } from "../Game";
import { Player } from "../Player";
import { Client } from "../Client";
import { Card } from "../Card";
import CardType from "../enum/CardType";
import { GS_Regular } from "./GS_Regular";

class GS_Combat extends GameState {
  private lastPlayer: Player;
  private cardsToDraw: number;

  constructor(game: Game, cardsToDraw: number, lastPlayer: Player) {
    super(game);
    this.cardsToDraw = cardsToDraw;
    this.lastPlayer = lastPlayer;
  }

  public removeClient(socketId: string): void {
    const room = this.getGame().getRoom();
    const newPlayers = room.getPlayers().filter((player: Player) => {
      if (player instanceof Client) {
        return (player.getSocket() !== socketId);
      } else {
        return true;
      }
    });
    room.setPlayers(newPlayers);

    // TODO: handle checking-up flags such as hasTurn, etc.
  }

  public isCardPlayable(card: Card): boolean {
    const topCard = this.getGame().getTopCard();
    return (
      card.isType(CardType.Combat) &&
      (
        card.getRank() === topCard.getRank() ||
        card.getSuit() === topCard.getSuit()
      )
    );
  }

  public playCard(card: Card): void {
    const game = this.getGame();
    const newStack = this.getGame().getStack();
    newStack.push(card);
    game.setStack(newStack);
  }

  public canPlayerDrawCard(player: Player): boolean {
    return (player.hasTurn() && !player.hasDrawnCard());
  }

  public canPlayerPassTurn(player: Player): boolean {
    return (player.hasTurn() && player.hasDrawnCard());
  }

  public passTurn(): void {
    const game = this.getGame();
    const topCard = game.getTopCard();
    const room = game.getRoom();
    const players = room.getPlayers();
    const currentPlayerObject = game.getCurrentPlayer();
    const playerPassingTurn = currentPlayerObject.object;
    let nextIndex = (currentPlayerObject.index + 1) % players.length;

    while (players[nextIndex].isWinner()) {
      nextIndex = (nextIndex + 1) % players.length;
    }

    const nextPlayer = players[nextIndex];

    game.setCurrentPlayer({
      index: nextIndex,
      object: nextPlayer
    });

    // if played card - the card must've been combat
    if (playerPassingTurn.hasPlayedCard()) {
      this.lastPlayer = playerPassingTurn;
      this.cardsToDraw += topCard.getCombatValue();
      game.setNextState(this);
      console.log("Combat: state remains (last player played combat card)");
    } else {
      if (playerPassingTurn.skipsTurn()) {
        if (nextPlayer.getName() === this.lastPlayer.getName()) {
          game.setNextState(new GS_Regular(game));
          console.log("Combat: state changed to Regular (cannot play combat card on self)");
        } else {
          game.setNextState(this);
          console.log("Combat: state remains (player skipped but next is not last)");
        }
      } else {
        if (playerPassingTurn.hasDrawnCard()) {
          game.setNextState(new GS_Regular(game));
          playerPassingTurn.setCardsToDraw(this.cardsToDraw - 1);
          console.log("Combat: state changed to Regular (last player drew a card and passed turn)");
        } else {
          // this block is active when player neither played a card
          // nor drew a card, i.e. only when he couldn't play any card
          // and there were no cards in the deck (might happen during intense
          // gameplay with many players)
          game.setNextState(new GS_Regular(game));
          console.log("Combat: state changed to Regular (player neither played nor drew card)");
        }
      }
    }

    playerPassingTurn.setTurnsToSkip(Math.max(playerPassingTurn.getTurnsToSkip() - 1, 0));
    playerPassingTurn.setDrawnCard(false);
    playerPassingTurn.setPlayedCard(false);
    playerPassingTurn.setHasTurn(false);
  }
}

export default GS_Combat;