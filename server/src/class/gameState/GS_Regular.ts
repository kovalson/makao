import { GameState } from "../GameState";
import { Player } from "../Player";
import { Client } from "../Client";
import { Card } from "../Card";
import CardType from "../enum/CardType";
import GS_Skip from "./GS_Skip";
import GS_RankDemand from "./GS_RankDemand";
import GS_Combat from "./GS_Combat";

export class GS_Regular extends GameState {
  public removeClient(socketId: string): void {
    const room = this.getGame().getRoom();
    const newPlayers = room.getPlayers().filter((player: Player) => {
      if (player instanceof Client) {
        return (player.getSocket() !== socketId);
      } else {
        return true;
      }
    });
    room.setPlayers(newPlayers);

    // TODO: handle checking-up flags such as hasTurn, etc.
  }

  public isCardPlayable(card: Card): boolean {
    const topCard = this.getGame().getTopCard();
    const ranksMatch = (card.getRank() === topCard.getRank());
    const suitsMatch = topCard.hasChangedSuit() ? (card.getSuit() === topCard.getChangedSuit()) : (card.getSuit() === topCard.getSuit());
    
    return (ranksMatch || suitsMatch);
  }

  public playCard(card: Card): void {
    const game = this.getGame();
    const newStack = this.getGame().getStack();
    newStack.push(card);
    game.setStack(newStack);
  }

  public canPlayerDrawCard(player: Player): boolean {
    return (player.hasTurn() && !player.hasDrawnCard());
  }

  public canPlayerPassTurn(player: Player): boolean {
    return (player.hasTurn() && player.hasDrawnCard());
  }

  public passTurn(): void {
    const game = this.getGame();
    const topCard = game.getTopCard();
    const room = game.getRoom();
    const players = room.getPlayers();
    const playerPassingTurn = game.getCurrentPlayer();
    let nextIndex = (playerPassingTurn.index + 1) % players.length;
    
    while (players[nextIndex].isWinner()) {
      nextIndex = (nextIndex + 1) % players.length;
    }
    
    game.setCurrentPlayer({
      index: nextIndex,
      object: players[nextIndex]
    });
    
    if (playerPassingTurn.object.hasPlayedCard() && topCard.isType(CardType.Skip)) {
      game.setNextState(new GS_Skip(game, playerPassingTurn.object));
      console.log("Regular: Changing state to Skip");
    } else if (playerPassingTurn.object.hasPlayedCard() && topCard.isType(CardType.RankDemanding)) {
      if (topCard.doesDemandRank()) {
        game.setNextState(new GS_RankDemand(game, topCard.getRankDemand()));
        console.log("Regular: Changing state to RankDemand");
      } else {
        game.setNextState(this);
      }
    } else if (playerPassingTurn.object.hasPlayedCard() && topCard.isType(CardType.Combat)) {
      game.setNextState(new GS_Combat(game, topCard.getCombatValue(), playerPassingTurn.object));
      console.log("Regular: Changing state to Combat");
    } else {
      game.setNextState(this);
    }

    playerPassingTurn.object.setDrawnCard(false);
    playerPassingTurn.object.setPlayedCard(false);
    playerPassingTurn.object.setHasTurn(false);
    playerPassingTurn.object.setTurnsToSkip(Math.max(playerPassingTurn.object.getTurnsToSkip() - 1, 0));
  }
}