import { GameState } from "../GameState";
import { Game } from "../Game";
import { Player } from "../Player";
import { Client } from "../Client";
import { Card } from "../Card";
import CardType from "../enum/CardType";
import { GS_Regular } from "./GS_Regular";

class GS_RankDemand extends GameState {
  private rankDemand: number;
  private turnsLeft: number;
  
  constructor(game: Game, rankDemand: number) {
    super(game);
    this.rankDemand = rankDemand;
    this.turnsLeft = game.getRoom().getPlayers().length;
  }

  public removeClient(socketId: string): void {
    const room = this.getGame().getRoom();
    const newPlayers = room.getPlayers().filter((player: Player) => {
      if (player instanceof Client) {
        return (player.getSocket() !== socketId);
      } else {
        return true;
      }
    });
    room.setPlayers(newPlayers);

    // TODO: handle checking-up flags such as hasTurn, etc.
  }

  public isCardPlayable(card: Card): boolean {
    return (card.getRank() === this.rankDemand || card.isType(CardType.RankDemanding));
  }

  public playCard(card: Card): void {
    const game = this.getGame();
    const newStack = this.getGame().getStack();
    newStack.push(card);
    game.setStack(newStack);
  }

  public canPlayerDrawCard(player: Player): boolean {
    return true;
  }

  public canPlayerPassTurn(player: Player): boolean {
    return (player.hasTurn() && player.hasDrawnCard());
  }

  public passTurn(): void {
    const game = this.getGame();
    const topCard = game.getTopCard();
    const room = game.getRoom();
    const players = room.getPlayers();
    const currentPlayerObject = game.getCurrentPlayer();
    const playerPassingTurn = currentPlayerObject.object;
    let nextIndex = (currentPlayerObject.index + 1) % players.length;

    while (players[nextIndex].isWinner()) {
      nextIndex = (nextIndex + 1) % players.length;
    }

    const nextPlayer = players[nextIndex];

    game.setCurrentPlayer({
      index: nextIndex,
      object: nextPlayer
    });

    if (playerPassingTurn.hasPlayedCard()) {
      if (topCard.isType(CardType.RankDemanding)) {
        game.setNextState(new GS_RankDemand(game, topCard.getRankDemand()));
        this.turnsLeft++;
        console.log("RankDemand: state remains (player played another rank-demanding card)");
      } else {
        game.setNextState(this);
        console.log("RankDemand: state remains (player played demanded card)");
      }
    } else {
      game.setNextState(this);
      console.log("RankDemand: state remains (player drew card)");
    }
    
    this.turnsLeft--;
    if (this.turnsLeft <= 0) {
      game.setNextState(new GS_Regular(game));
      console.log("!! RankDemand: state changes to Regular (turns over)")
    }

    playerPassingTurn.setDrawnCard(false);
    playerPassingTurn.setPlayedCard(false);
    playerPassingTurn.setHasTurn(false);
    playerPassingTurn.setTurnsToSkip(Math.max(playerPassingTurn.getTurnsToSkip() - 1, 0));
  }
}

export default GS_RankDemand;