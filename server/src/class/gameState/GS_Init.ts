import { GameState } from "../GameState";
import { Game } from "../Game";
import { Player } from "../Player";
import { Card } from "../Card";
import Suit from "../enum/Suit";
import { GS_Regular } from "./GS_Regular";

export class GS_Init extends GameState {
  private readonly CARDS_TO_DEAL = 5;

  constructor(game: Game) {
    super(game);

    this.initPlayers();
    this.initStack();
    // this.scenarioCombatCards();
    // this.scenarioRankDemand();
    // this.scenarioSkipCards();
    // this.scenarioBotSuitChange();
    // this.fastGame();
    this.initDeck();
    this.shuffleDeck();
    this.dealCards();
    this.initTopCard();
    this.initCurrentPlayer();
    
    game.setNextState(new GS_Regular(game));
  }

  private initPlayers(): void {
    const room = this.getGame().getRoom();
    room.getPlayers().forEach((player: Player) => {
      player.setReadyToPlay(false);
      player.setHasTurn(false);
      player.setCards([]);
    });
  }

  private initStack(): void {
    this.getGame().setStack([]);
  }

  private scenarioRankDemand(): void {
    this.getGame().setDeck([
      new Card(11, Suit.Spades),
      new Card(2, Suit.Hearts),

      new Card(10, Suit.Clubs),
      new Card(3, Suit.Hearts),
      
      new Card(9, Suit.Spades),
      new Card(5, Suit.Clubs),
      
      new Card(8, Suit.Clubs),
      new Card(5, Suit.Hearts),

      new Card(7, Suit.Spades),
      new Card(3, Suit.Clubs),

      new Card(10, Suit.Spades),
      
      new Card(12, Suit.Hearts),
      new Card(12, Suit.Clubs),
      new Card(12, Suit.Diamonds),
      new Card(13, Suit.Diamonds),
      new Card(13, Suit.Hearts)
    ].reverse());
  }

  private scenarioSkipCards(): void {
    this.getGame().setDeck([
      new Card(4, Suit.Hearts),
      new Card(4, Suit.Spades),
      new Card(4, Suit.Clubs),
      new Card(5, Suit.Hearts),
      new Card(4, Suit.Diamonds),
      new Card(3, Suit.Clubs),
      new Card(6, Suit.Clubs),
      new Card(7, Suit.Clubs),
      new Card(8, Suit.Clubs),
      new Card(9, Suit.Clubs),
      new Card(10, Suit.Clubs),
      new Card(11, Suit.Clubs),
      new Card(12, Suit.Clubs),
      new Card(13, Suit.Clubs)
    ].reverse());
  }

  private scenarioBotSuitChange(): void {
    this.getGame().setDeck([

      // player gets some random cards
      // bot gets Ace of hearts and some "hearts"-cards and random too
      new Card(14, Suit.Hearts),
      new Card(2, Suit.Clubs),
      new Card(3, Suit.Clubs),
      new Card(3, Suit.Hearts),
      new Card(4, Suit.Clubs),
      new Card(4, Suit.Spades),
      new Card(5, Suit.Clubs),
      new Card(5, Suit.Spades),
      new Card(6, Suit.Clubs),
      new Card(6, Suit.Spades),

      // 11th card goes on stack
      new Card(7, Suit.Hearts),

      // three random cards
      new Card(9, Suit.Spades),
      new Card(10, Suit.Spades),
      new Card(11, Suit.Spades),
    ].reverse());
  }

  private fastGame(): void {
    this.getGame().setDeck([
      new Card(2, Suit.Clubs),
      new Card(3, Suit.Clubs),
      new Card(4, Suit.Clubs),
      new Card(5, Suit.Clubs),
      new Card(6, Suit.Clubs),
      new Card(7, Suit.Clubs),
      new Card(8, Suit.Clubs),
      new Card(9, Suit.Clubs),
      new Card(10, Suit.Clubs),
      new Card(11, Suit.Clubs),
      new Card(12, Suit.Clubs),
      new Card(13, Suit.Clubs),
      new Card(14, Suit.Clubs)
    ]);
  }

  private initDeck(): void {
    let cards: Array<Card> = [];
    for (let rank = 2; rank < 15; rank++) {
      cards.push(new Card(rank, Suit.Clubs));
      cards.push(new Card(rank, Suit.Diamonds));
      cards.push(new Card(rank, Suit.Hearts));
      cards.push(new Card(rank, Suit.Spades));
    }
    this.getGame().setDeck(cards);
  }

  private shuffleDeck(): void {
    let deck = this.getGame().getDeck();
    let newDeck: Array<Card> = [];
    
    while (deck.length > 0) {
      const index = (Math.random() * deck.length) | 0;
      newDeck.push(deck.splice(index, 1)[0]);
    }

    this.getGame().setDeck(newDeck);
  }

  private dealCards(): void {
    const players: Array<Player> = this.getGame().getRoom().getPlayers();
    let cardsToDeal = this.CARDS_TO_DEAL;

    while (cardsToDeal--) {
      players.forEach((player: Player) => {
        player.addCard(this.getGame().drawCard());
      });
    }
  }

  private initTopCard(): void {
    this.getGame().setStack([this.getGame().drawCard()]);
  }

  private initCurrentPlayer(): void {
    const players = this.getGame().getRoom().getPlayers();
    // const randomId = Math.floor(Math.random() * players.length);
    const randomId = 0;
    const player = players[randomId];
    
    player.setHasTurn(true);

    this.getGame().setCurrentPlayer({
      index: randomId,
      object: player
    });
  }

  public removeClient(socketId: string): void {
    console.log("Shieet");
  }
}