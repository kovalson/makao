import { Room } from "./Room";
import { GameState } from "./GameState";
import { Card } from "./Card";
import { GS_Init } from "./gameState/GS_Init";
import { Player } from "./Player";

interface ICurrentPlayer {
  index: number,
  object: Player
}

export class Game {
  private room: Room;
  private state: GameState;
  private nextState: GameState;
  private deck: Array<Card>;
  private stack: Array<Card>;
  private currentPlayer: ICurrentPlayer;
  private firstMove: boolean;
  
  constructor(room: Room) {
    this.room = room;
    this.firstMove = true;
    this.setState(new GS_Init(this));
    this.updateState();
  }

  public getRoom(): Room {
    return this.room;
  }

  public getState(): GameState {
    return this.state;
  }

  public setState(state: GameState): void {
    this.state = state;
  }

  public setNextState(nextState: GameState): void {
    this.nextState = nextState;
  }

  public updateState(): void {
    if (!!this.nextState) {
      this.state = this.nextState;
      this.nextState = null;
      this.updatePlayersCards();
    }
  }

  public getCurrentPlayer(): ICurrentPlayer {
    return this.currentPlayer;
  }

  public setCurrentPlayer(currentPlayer: ICurrentPlayer): void {
    this.currentPlayer = currentPlayer;
    this.currentPlayer.object.setHasTurn(true);
  }

  public getDeck(): Array<Card> {
    return this.deck;
  }

  public setDeck(cards: Array<Card>): void {
    this.deck = cards;
  }

  /**
   * Pops a card from the deck and returns it.
   * If the deck is empty (which might happen when
   * players drew all cards) moves all cards (except last one)
   * from the stack to the deck. Then shuffles the deck,
   * pops a card and returns it. If there are still no cards
   * after moving them from the stack, returns undefined (as
   * does the pop() method).
   * @returns {Card | undefined}
   */
  public drawCard(): Card {
    this.firstMove = false;
    const card = this.deck.pop();
    if (card) {
      return card;
    } else {
      this.moveStackToDeck();
      return this.deck.pop();
    }
  }

  public getStack(): Array<Card> {
    return this.stack;
  }

  public setStack(cards: Array<Card>): void {
    this.stack = cards;
  }

  public isCardPlayable(card: Card): boolean {
    return this.state.isCardPlayable(card);
  }

  public playCard(card: Card): void {
    this.firstMove = false;
    this.state.playCard(card);
    this.updateState();
  }

  public canCurrentPlayerDrawCard(): boolean {
    return this.state.canPlayerDrawCard(this.currentPlayer.object);
  }

  public moveStackToDeck(): boolean {
    if (this.stack.length === 1) {
      return false;
    }
    this.deck = [...this.stack];
    this.deck.pop();
    this.stack = [this.stack.pop()];
    this.resetCardsSpecialFlags();
    this.shuffleDeck();
    return true;
  }

  private resetCardsSpecialFlags(): void {
    this.getDeck().forEach((card: Card) => {
      card.resetSpecialFlags();
    });
  }

  private shuffleDeck(): void {
    const shuffled: Array<Card> = [];
    const deck = this.deck;
    while (deck.length > 0) {
      const index = Math.random() * deck.length | 0;
      shuffled.push(deck.splice(index, 1)[0]);
    }
    this.deck = shuffled;
  }

  public getTopCard(): Card {
    const stack = this.stack;
    
    if (!stack.length) {
      return null;
    }

    return stack[stack.length - 1];
  }

  public peekDeck(): Card {
    const {deck} = this;
    if (deck.length === 0) {
      return null;
    }
    return deck[deck.length - 1];
  }

  public isDeckEmpty(): boolean {
    return (this.deck.length === 0);
  }

  public updatePlayersCards(): void {
    const players = this.getRoom().getPlayers();
    players.forEach((player) => {
      player.getCards().forEach((card) => {
        card.setPlayable(this.isCardPlayable(card));
      });
    });
  }

  public updatePlayersCanPassTurn(): void {
    const players = this.getRoom().getPlayers();
    const state = this.state;
    players.forEach((player) => {
      player.setCanPassTurn(state.canPlayerPassTurn(player));
    });
  }

  public updatePlayersCanDrawCard(): void {
    const players = this.getRoom().getPlayers();
    const state = this.state;
    players.forEach((player) => {
      player.setCanDrawCard(state.canPlayerDrawCard(player));
    });
  }

  public passTurn(): void {
    this.state.passTurn();
    this.updateState();
  }
  
  public isOver(): boolean {
    return (this.getRoom().getPlayers().filter((player: Player) => {
      return !player.isWinner()
    }).length === 1);
  }
}