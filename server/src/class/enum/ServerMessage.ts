enum ServerMessage {
  Listening = "listening",

  NameIsAlreadyTaken = "name is already taken",
  NameChangedSuccessfully = "name changed successfully",
  ClientDoesNotExist = "client does not exist",

  RoomSuccessfullyCreated = "room successfully created",
  RoomInputInvalid = "room input invalid",
  ClientAlreadyOwnsARoom = "client already owns a room",

  RoomsList = "rooms list",
  RoomIsFull = "room is full",
  ClientAlreadyInRoom = "client already in room",
  SuccessfullyJoinedRoom = "successfully joined room",
  RoomSuccessfullyLeft = "room successfully left",
  
  RoomUpdate = "room update",
  NewRoom = "new room",
  RoomDelete = "room delete",

  ClientIsReadyToPlay = "client is ready to play",
  UpdateClientReadyToPlayStatus = "update client ready to play status",
  GameStarted = "game started",
  PlayerCards = "player cards",
  StackUpdate = "stack update",
  DeckUpdate = "deck update",
  PlayerUpdate = "player update",
  RemoveCard = "remove card",
  CardsUpdate = "cards update",
  CardDrawn = "card drawn",
  TurnUpdate = "turn update",
  RevealCard = "reveal card",
  GameUpdate = "game update",
  ChangeStackCardSuit = "change stack card suit",
  ShowCardDemand = "show card demand",

  Win = "win",
  GameOver = "game over"
}

export default ServerMessage;