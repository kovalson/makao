enum ClientMessage {
  Connection = "connection",
  Disconnect = "disconnect",
  
  Name = "client name",
  
  CreateRoom = "create room",
  GetRooms = "get rooms",
  GetRoomInfo = "get room info",
  JoinRoom = "join room",
  LeaveRoom = "leave room",

  SetReady = "set ready",
  AddBot = "add bot",
  RemoveFromRoom = "remove from room",
  GetCards = "get cards",
  PlayCard = "play card",
  DrawCard = "draw card",
  PassTurn = "pass turn"
}

export default ClientMessage;