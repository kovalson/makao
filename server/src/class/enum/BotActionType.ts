export enum BotActionType {
  PlayCard,
  DrawCard,
  PassTurn
}