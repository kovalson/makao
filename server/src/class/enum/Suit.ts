enum Suit {
  Spades = "Spades",
  Hearts = "Hearts",
  Diamonds = "Diamonds",
  Clubs = "Clubs"
}

namespace Suit {
  export function fromObject(suit: string): Suit {
    return Suit[suit];
  }
}

export default Suit;