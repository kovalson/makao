import Suit from "./enum/Suit";
import CardType from "./enum/CardType";

export class Card {
  public static Type(rank: number, suit: Suit): CardType {
    if (rank === 2 || rank === 3 || (rank === 13 && (suit === Suit.Spades || suit === Suit.Hearts))) {
      return CardType.Combat;
    } else if (rank === 4) {
      return CardType.Skip;
    } else if (rank === 14) {
      return CardType.SuitChanging;
    } else if (rank === 11) {
      return CardType.RankDemanding;
    } else {
      return CardType.Regular;
    }
  }

  private rank: number;
  private suit: Suit;
  private playable: boolean;
  private type: CardType;
  
  // flag specific for suit-changing cards
  private changedSuit: Suit;
  
  // flag specific for rank-demanding cards
  private rankDemand: number;

  // flag specific for combat cards
  private combatValue: number;

  constructor(rank: number, suit: Suit) {
    this.rank = rank;
    this.suit = suit;
    this.playable = false;
    this.type = Card.Type(rank, suit);

    this.changedSuit = suit;
    this.rankDemand = 0;
    this.combatValue = Math.min(this.rank, 5);
  }

  public getRank(): number {
    return this.rank;
  }

  public getSuit(): Suit {
    return this.suit;
  }

  public isPlayable(): boolean {
    return this.playable;
  }

  public setPlayable(bool: boolean): void {
    this.playable = bool;
  }

  public getType(): CardType {
    return this.type;
  }

  public isType(type: CardType): boolean {
    return (this.type === type);
  }

  public changeSuit(suit: Suit): void {
    this.changedSuit = suit;
  }

  public hasChangedSuit(): boolean {
    return (this.type === CardType.SuitChanging && this.changedSuit !== this.suit);
  }

  public getChangedSuit(): Suit {
    return this.changedSuit;
  }

  public getRankDemand(): number {
    return this.rankDemand;
  }

  public setRankDemand(rank: number): void {
    this.rankDemand = rank;
  }

  public doesDemandRank(): boolean {
    return (this.rankDemand !== 0);
  }

  public getCombatValue(): number {
    return this.combatValue;
  }

  public resetSpecialFlags(): void {
    this.changedSuit = this.suit;
    this.rankDemand = 0;
  }

  public getSendable(): object {
    return {
      rank: this.rank,
      suit: this.suit.toString(),
      type: this.type,
      playable: this.playable,
      changedSuit: this.changedSuit,
      rankDemand: this.rankDemand
    };
  }
}