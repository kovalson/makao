import { Card } from "./Card";

export class CardsUpdate {
  public static Add(cards: Array<Card>): object {
    return CardsUpdate.Get("add", cards);
  }

  public static Set(cards: Array<Card>): object {
    return CardsUpdate.Get("set", cards);
  }

  public static Remove(card: Card): object {
    return CardsUpdate.Get("remove", card);
  }

  public static Get(type: string, cards: Array<Card> | Card): object {
    if (Array.isArray(cards)) {
      return {
        type: type,
        cards: cards.map((card) => card.getSendable())
      };
    } else {
      return {
        type: type,
        card: cards.getSendable()
      }
    }
  }
}