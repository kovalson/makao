import { Player } from "./Player";
import { BotAction } from "./BotAction";
import { Game } from "./Game";
import { Card } from "./Card";
import CardType from "./enum/CardType";
import Suit from "./enum/Suit";
import GS_Skip from "./gameState/GS_Skip";

export class Bot extends Player {
  public static readonly NAMES: Array<string> = [
    "Juan", "Jorge", "Rodriguez", "Perez", "Jesus",
    "Manuel", "Velasquez", "Roberto", "Martinez",
    "Gonzalez", "Fernando", "Samuel", "Diego", "Pablo",
    "Andres", "Rodrigo", "Carlos", "Valentino",
    "Esteban", "Eduardo"
  ];
  
  public static Random() {
    const name: string = Bot.NAMES[Math.floor(Math.random() * Bot.NAMES.length)];
    return new Bot(name);
  }
  
  constructor(name: string) {
    super(name);
    this.setReadyToPlay(true);
  }
  
  public setRandomName(): void {
    const name: string = Bot.NAMES[Math.floor(Math.random() * Bot.NAMES.length)];
    this.setName(name);
  }

  public resetGameFlags(): void {
    this.setCards([]);
    this.setHasTurn(false);
    this.setReadyToPlay(true);
  }

  private getFirstPlayableCard(): Card {
    const cards = this.getCards();
    for (let card of cards) {
      if (card.isPlayable()) {
        return card;
      }
    }
    return null;
  }

  private getMostFrequentCardsSuit(): Suit {
    return Array.from(this.getCards().reduce((map, element) => map.set(element.getSuit(), (map.get(element.getSuit()) || 0) + 1), new Map<Suit, number>()))
    .reduce((a, b) => {
      if (a[1] >= b[1]) return a;
      else return b;
    })[0];
  }

  private getFirstRegularRank(): number {
    const card = this.getCards().find((card: Card) => card.isType(CardType.Regular));
    if (card) {
      return card.getRank();
    } else {
      return 0;
    }
  }

  public getAction(game: Game): BotAction {
    const playableCard = this.getFirstPlayableCard();
    if (playableCard) {
      // handle suit-changing card
      if (playableCard.isType(CardType.SuitChanging)) {
        const changedSuit = this.getMostFrequentCardsSuit();
        playableCard.changeSuit(changedSuit);
      } else if (playableCard.isType(CardType.RankDemanding)) {
        const rankDemand = this.getFirstRegularRank();
        playableCard.setRankDemand(rankDemand);
      }
      return BotAction.PlayCard(playableCard);
    } else if (game.getState() instanceof GS_Skip) {
      return BotAction.PassTurn();
    } else {
      return BotAction.DrawCard();
    }
  }

  public isBot(): boolean {
    return true;
  }
}