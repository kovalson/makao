export class InvalidRoomInputError extends Error {
    constructor(message?: string) {
        super(message);
    }
}