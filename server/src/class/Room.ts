import { InvalidRoomInputError } from "./error/InvalidRoomInputError";
import { Client } from "./Client";
import { RoomState } from "./RoomState";
import { RS_Awaiting } from "./roomState/RS_Awaiting";
import { Player } from "./Player";
import { ResponseMessage } from "./ResponseMessage";
import ServerMessage from "./enum/ServerMessage";
import { Bot } from "./Bot";

export class Room {
  public static fromObject(object: any, owner: Client): Room {
    const name = object.name;
    const playersNumber = object.playersNumber;

    if (
      !name ||
      !playersNumber ||
      
      name.constructor !== String ||
      playersNumber.constructor !== Number ||
      
      name.length < 3 ||
      /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(name) ||
      playersNumber <= 1
    ) {
      throw new InvalidRoomInputError();
    }

    return new Room(name, playersNumber, owner);
  }

  private name: string;
  private playersNumber: number;
  private players: Array<Player>;
  private owner: Client;
  private state: RoomState;

  constructor(name: string, playersNumber: number, owner: Client) {    
    this.name = name;
    this.playersNumber = playersNumber;
    this.players = [];
    this.owner = owner;
    this.state = new RS_Awaiting(this);
  }

  public addPlayer(player: Player): void {
    this.players.push(player);
  }

  public addClient(client: Client): ResponseMessage {
    if (this.isFull()) {
      return ResponseMessage.Failure(ServerMessage.RoomIsFull);
    } else {
      return this.state.addClient(client);
    }
  }

  public removeClient(socketId: string): void {
    this.state.removeClient(socketId);
  }

  public removeBots(): void {
    this.players = this.players.filter((player: Player) => {
      return !(player instanceof Bot);
    });
  }

  public getPlayerByName(name: string): Player {
    return this.players.find((player: Player) => player.getName() === name);
  }

  public removePlayerByName(name: string): void {
    this.players = this.players.filter((player) => player.getName() !== name);
  }

  public isReadyToStart(): boolean {
    return (
      this.isEveryoneReady() &&
      this.containsClient() &&
      this.getPlayers().length > 1
    );
  }

  public isEveryoneReady(): boolean {
    return !this.players.find((player: Player) => !player.isReadyToPlay());
  }

  public containsClient(): boolean {
    return !!this.players.find((player: Player) => !player.isBot());
  }

  public isFull(): boolean {
    return (this.players.length >= this.playersNumber);
  }

  public isEmpty(): boolean {
    return (this.players.length === 0);
  }

  public getName(): string {
    return this.name;
  }

  public setState(state: RoomState): void {
    this.state = state;
  }

  public getState(): RoomState {
    return this.state;
  }

  public updateState(): boolean {
    return this.state.update();
  }

  public getOwner(): Client {
    return this.owner;
  }

  public getPlayers(): Array<Player> {
    return this.players;
  }

  public setPlayers(players: Array<Player>): void {
    this.players = players;
  }

  public getClients(): Array<Client> {
    const clients: Array<Client> = [];
    for (let player of this.players) {
      if (player instanceof Client) {
        clients.push(player);
      }
    }
    return clients;
  }

  public getBots(): Array<Bot> {
    const bots: Array<Bot> = [];
    for (let player of this.players) {
      if (player instanceof Bot) {
        bots.push(player);
      }
    }
    return bots;
  }

  public resetPlayersGameFlags(): void {
    this.players.forEach((player: Player) => player.resetGameFlags());
  }

  public hasPlayerWithName(name: string): boolean {
    return !!this.players.find((player: Player) => player.getName() === name);
  }

  public hasOnlyBots(): boolean {
    return !this.players.find((player: Player) => !player.isBot());
  }

  public hasOneClientLeft(): boolean {
    return (
      this.players.length === 1 &&
      this.players[0] instanceof Client  
    );
  }

  public getGeneralInfo(): object {
    return {
      name: this.name,
      playersNumber: this.playersNumber,
      currentPlayersNumber: this.players.length,
      owner: this.owner.getSendable(),
      state: this.state.getSendable()
    };
  }

  public getDetails(): object {
    return {
      name: this.name,
      playersNumber: this.playersNumber,
      currentPlayersNumber: this.players.length,
      players: this.players.map((player: Player) => player.getSendable()),
      owner: this.owner.getSendable(),
      state: this.state.getSendable()
    };
  }
}