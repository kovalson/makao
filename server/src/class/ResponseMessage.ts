export class ResponseMessage {
  public static Success(): ResponseMessage {
    return new ResponseMessage(true);
  }

  public static Failure(message: string): ResponseMessage {
    return new ResponseMessage(false, message);
  }
  
  private status: boolean;
  private message?: string;
  
  constructor(status: boolean, message?: string) {
    this.status = status;
    this.message = message;
  }

  public isSuccess(): boolean {
    return this.status;
  }

  public isFailure(): boolean {
    return !this.status;
  }

  public getMessage(): string {
    return this.message;
  }
}