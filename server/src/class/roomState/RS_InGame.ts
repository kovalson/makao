import { RoomState } from "../RoomState";
import { Room } from "../Room";
import { Game } from "../Game";

export class RS_InGame extends RoomState {
  private game: Game;

  constructor(room: Room) {
    super(room);
    this.game = new Game(room);
  }

  public removeClient(socketId: string): void {
    this.game.getState().removeClient(socketId);
  }

  public getGame(): Game {
    return this.game;
  }

  public getSendable(): object {
    return {
      name: "In-game"
    };
  }
}