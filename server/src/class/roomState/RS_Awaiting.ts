import { Room } from "../Room";
import { RoomState } from "../RoomState";
import { Client } from "../Client";
import { ResponseMessage } from "../ResponseMessage";
import { Player } from "../Player";

export class RS_Awaiting extends RoomState {
  constructor(room: Room) {
    super(room);
  }

  public addClient(client: Client): ResponseMessage {
    this.getRoom().addPlayer(client);
    return ResponseMessage.Success();
  }

  public removeClient(socketId: string): void {
    const room = this.getRoom();
    const newPlayers = room.getPlayers().filter((player: Player) => {
      if (player instanceof Client) {
        return (player.getSocket() !== socketId);
      } else {
        return true;
      }
    });
    room.setPlayers(newPlayers);
  }

  public getSendable(): object {
    return {
      name: "Awaiting players..."
    };
  }
}