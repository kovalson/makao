import { RoomState } from "../RoomState";
import { Room } from "../Room";
import { Client } from "../Client";
import { RS_Awaiting } from "./RS_Awaiting";
import { ResponseMessage } from "../ResponseMessage";

export class RS_Timeout extends RoomState { 
  // default time to room timeout (in seconds)
  readonly DEFAULT_TIME_LEFT: number = 5;
  
  // time left to room timeout (in seconds)
  private timeLeft: number;
  
  constructor(room: Room) {
    super(room);
    this.timeLeft = this.DEFAULT_TIME_LEFT;
  }

  public addClient(client: Client): ResponseMessage {
    const room = this.getRoom();
    room.addPlayer(client);
    room.setState(new RS_Awaiting(room));
    return ResponseMessage.Success();
  }

  /**
   * Decrements timeLeft. If the time is over - returns false.
   * If the time is not over yet - returns true.
   */
  public update(): boolean {
    this.timeLeft--;
    return (this.timeLeft > 0);
  }

  public getSendable(): object {
    return {
      name: "Expires in " + this.timeLeft + " seconds"
    };
  }
}