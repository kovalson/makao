import { Player } from "./Player";
import { Room } from "./Room";
import { ResponseMessage } from "./ResponseMessage";
import ServerMessage from "./enum/ServerMessage";

export class Client extends Player {
  private socket: string;
  private room: Room;

  constructor(socket: string, name: string = "") {
    super(name);
    this.socket = socket;
    this.room = null;
  }

  public joinRoom(room: Room): ResponseMessage {
    if (this.isInRoom()) {
      return ResponseMessage.Failure(ServerMessage.ClientAlreadyInRoom);
    } else {
      this.room = room;
      return ResponseMessage.Success();
    }
  }

  public leaveRoom(): void {
    if (!this.isInRoom()) {
      return;
    }

    this.resetGameFlags();
    this.room.removeClient(this.socket);
    this.room = null;
  }

  public isInRoom(): boolean {
    return (this.room !== null);
  }

  public setRoom(room: Room): void {
    this.room = room;
  }

  public getRoom(): Room {
    return this.room;
  }

  public getSocket(): string {
    return this.socket;
  }

  public getSendable(): object {
    return {
      name: this.getName(),
      socket: this.socket,
      control: this.constructor.name,
      readyToPlay: this.isReadyToPlay(),
      winner: this.isWinner(),
      playedCard: this.hasPlayedCard(),
      canPassTurn: this.canPassTurn(),
      canDrawCard: this.canDrawCard(),
      drawnCard: this.hasDrawnCard(),
      hasTurn: this.hasTurn()
    };
  }
}