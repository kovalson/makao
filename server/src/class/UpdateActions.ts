import ServerMessage from "./enum/ServerMessage";

interface IAction {
  type: ServerMessage,
  data?: any
}

export class UpdateActions {
  public actions: Array<IAction>;
  
  constructor() {
    this.actions = [];
  }

  public add(type: ServerMessage, data?: any): void {
    this.actions.push({ type, data });
  }

  public getActions(): Array<IAction> {
    return this.actions;
  }
}