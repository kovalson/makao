import { Game } from "./Game";
import { Player } from "./Player";
import { Card } from "./Card";

export class GameState {
  private game: Game;
  
  constructor(game: Game) {
    this.game = game;
  }

  public getGame(): Game {
    return this.game;
  }

  public removeClient(socketId: string): void {
    console.log("Shieet doubled");
    // do nothing
  }

  public isCardPlayable(card: Card): boolean {
    return false;
  }

  public playCard(card: Card): void {
    // do nothing
  }

  public canPlayerDrawCard(player: Player): boolean {
    return false;
  }

  public canPlayerPassTurn(player: Player): boolean {
    return false;
  }

  public passTurn(): void {
    // do nothing
  }
}