import { Room } from "./Room";
import ServerMessage from "./enum/ServerMessage";
import { Logger } from "./Logger";
import { SocketRoom } from "./enum/SocketRoom";
import { RS_Timeout } from "./roomState/RS_Timeout";

export class RoomsContainer {
  private rooms: Map<string, Room>;
  private intervals: Map<string, NodeJS.Timeout>;

  constructor() {
    this.rooms = new Map();
    this.intervals = new Map();
  }

  public setRoomTimeout(room: Room, io: any): void {
    const interval = this.createRoomTimeoutInterval(room, io);
    const roomSocket = room.getOwner().getSocket();

    this.clearRoomTimeoutInterval(room);
    this.intervals.set(roomSocket, interval);
    room.setState(new RS_Timeout(room));
  }

  private createRoomTimeoutInterval(room: Room, io: any): NodeJS.Timeout {
    return setInterval(() => {
      if (!room.updateState()) {
        const roomSocket = room.getOwner().getSocket();
        this.rooms.delete(roomSocket);
        this.clearRoomTimeoutInterval(room);

        io.to(SocketRoom.RoomsList).emit(ServerMessage.RoomDelete, roomSocket);
        Logger.roomTimedOut(room.getOwner(), room.getName());
      } else {
        io.to(SocketRoom.RoomsList).emit(ServerMessage.RoomUpdate, room.getDetails());
      }
    }, 1000);
  }

  public clearRoomTimeoutInterval(room: Room): void {
    const roomSocket = room.getOwner().getSocket();
    const interval = this.intervals.get(roomSocket);
    clearInterval(interval);
    this.intervals.delete(roomSocket);
  }

  public addRoom(room: Room): void {
    const socketId = room.getOwner().getSocket();
    this.rooms.set(socketId, room);
  }

  public getRoom(socketId: string): Room {
    return this.rooms.get(socketId);
  }

  public getRoomsArray(): Array<Room> {
    return Array.from(this.rooms.values());
  }

  public getRoomsList(): Array<object> {
    return Array.from(this.rooms.values()).map((room: Room) => room.getGeneralInfo());
  }
}