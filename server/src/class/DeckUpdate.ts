export class DeckUpdate {
  public static Set(amount: number): object {
    return DeckUpdate.Get("set", amount);
  }

  public static Add(amount: number): object {
    return DeckUpdate.Get("add", amount);
  }

  public static Remove(amount: number): object {
    return DeckUpdate.Get("remove", amount);
  }

  public static Get(type: string, amount: number): object {
    return {
      type: type,
      amount: amount
    };
  }
}