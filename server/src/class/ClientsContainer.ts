import { Client } from "./Client";

export class ClientsContainer {
  private clients: Map<string, Client>;

  constructor() {
    this.clients = new Map();
  }

  public addClient(client: Client): void {
    const socket = client.getSocket();
    this.clients.set(socket, client);
  }

  public removeClient(socketId: string): void {
    this.clients.delete(socketId);
  }

  public getClient(socketId: string): Client {
    return this.clients.get(socketId);
  }

  public hasClientWithName(name: string): boolean {
    const values: Array<Client> = Array.from(this.clients.values());
    return !!values.find((client) => client.getName() === name);
  }
}