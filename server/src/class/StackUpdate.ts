import { Card } from "./Card";

export class StackUpdate {
  public static Add(cards: Array<Card>): object {
    return StackUpdate.Get("add", cards);
  }

  public static Set(cards: Array<Card>): object {
    return StackUpdate.Get("set", cards);
  }

  public static Remove(): object {
    return StackUpdate.Get("remove");
  }

  public static Get(type: string, cards?: Array<Card> | number): object {
    if (Array.isArray(cards)) {
      return {
        type: type,
        cards: cards.map((card) => card.getSendable())
      };
    } else {
      return {
        type: type,
        amount: cards
      }
    }
  }
}