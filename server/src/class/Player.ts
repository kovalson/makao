import { Card } from "./Card";

export abstract class Player {
  private name: string;

  private readyToPlay: boolean;
  
  private winner: boolean;
  private cards: Array<Card>;
  private _hasTurn: boolean;
  private _canPassTurn: boolean;
  private _canDrawCard: boolean;
  private playedCard: boolean;
  private drawnCard: boolean;
  private turnsToSkip: number;
  private cardsToDraw: number;

  constructor(name: string) {
    this.name = name;
    this.resetGameFlags();
  }

  public getName() {
    return this.name;
  }

  public setName(name: string) {
    this.name = name;
  }

  public isReadyToPlay(): boolean {
    return this.readyToPlay;
  }

  public setReadyToPlay(bool: boolean): void {
    this.readyToPlay = bool;
  }

  public isBot(): boolean {
    return false;
  }

  public getCards(): Array<Card> {
    return this.cards;
  }

  public setCards(cards: Array<Card>): void {
    this.cards = cards;
  }

  public addCard(card: Card): void {
    this.cards.push(card);
  }

  public removeCardByIndex(index: number): boolean {
    return !!this.cards.splice(index, 1);
  }

  public hasTurn(): boolean {
    return this._hasTurn;
  }

  public setHasTurn(bool: boolean): void {
    this._hasTurn = bool;
  }

  public hasPlayedCard(): boolean {
    return this.playedCard;
  }

  public setPlayedCard(bool: boolean): void {
    this.playedCard = bool;
  }

  public setCanPassTurn(bool: boolean): void {
    this._canPassTurn = bool;
  }

  public canPassTurn(): boolean {
    return this._canPassTurn;
  }

  public canDrawCard(): boolean {
    return this._canDrawCard;
  }

  public setCanDrawCard(bool: boolean): void {
    this._canDrawCard = bool;
  }

  public hasDrawnCard(): boolean {
    return this.drawnCard;
  }

  public setDrawnCard(bool: boolean): void {
    this.drawnCard = bool;
  }

  public isWinner(): boolean {
    return this.winner;
  }

  public setWinner(bool: boolean): void {
    this.winner = bool;
  }

  public skipsTurn(): boolean {
    return (this.turnsToSkip > 0);
  }

  public setTurnsToSkip(value: number): void {
    this.turnsToSkip = value;
  }

  public getTurnsToSkip(): number {
    return this.turnsToSkip;
  }

  public hasCardsToDraw(): boolean {
    return (this.cardsToDraw > 0);
  }

  public getCardsToDraw(): number {
    return this.cardsToDraw;
  }

  public setCardsToDraw(value: number): void {
    this.cardsToDraw = value;
  }

  public resetGameFlags(): void {
    this.readyToPlay = false;

    this.cards = [];

    this.winner = false;
    this._hasTurn = false;
    this._canPassTurn = false;
    this._canDrawCard = false;
    this.playedCard = false;
    this.drawnCard = false;
    this.turnsToSkip = 0;
    this.cardsToDraw = 0;
  }

  public getCardByObject(object: any): Card {
    return this.cards.find((card: Card) => {
      return (object.rank === card.getRank() && object.suit === card.getSuit().toString());
    });
  }

  public removeCard(that: Card): void {
    this.cards = this.cards.filter((card: Card) => {
      return !(card.getRank() === that.getRank() && card.getSuit().toString() === that.getSuit().toString());
    });
  }

  public hasCards(): boolean {
    return (this.cards.length > 0);
  }

  public hasPlayableCards(): boolean {
    return !!this.cards.find((card) => card.isPlayable());
  }

  public getSendable(): object {
    return {
      name: this.getName(),
      control: this.constructor.name,
      readyToPlay: this.isReadyToPlay(),
      winner: this.isWinner(),
      playedCard: this.hasPlayedCard(),
      canPassTurn: this.canPassTurn(),
      canDrawCard: this.canDrawCard(),
      drawnCard: this.hasDrawnCard(),
      hasTurn: this._hasTurn,
      skipsTurn: this.skipsTurn()
    };
  }
}