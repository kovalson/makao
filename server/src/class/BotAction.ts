import { Card } from "./Card";
import { BotActionType } from "./enum/BotActionType";

export class BotAction {
  public static PassTurn(): BotAction {
    return new BotAction(BotActionType.PassTurn);
  }

  public static PlayCard(card: Card): BotAction {
    return new BotAction(BotActionType.PlayCard, card);
  }

  public static DrawCard(): BotAction {
    return new BotAction(BotActionType.DrawCard);
  }

  private type: BotActionType;
  private card?: Card;
  
  constructor(type: BotActionType, card?: Card) {
    this.type = type;
    this.card = card;
  }

  public getType(): BotActionType {
    return this.type;
  }

  public getCard(): Card {
    return this.card;
  }

  public is(type: BotActionType): boolean {
    return (this.type === type);
  }
}