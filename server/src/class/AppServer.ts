import * as io from "socket.io";
import { Logger } from "./Logger";
import { ClientsContainer } from "./ClientsContainer";
import { RoomsContainer } from "./RoomsContainer";
import ClientMessage from "./enum/ClientMessage";
import ServerMessage from "./enum/ServerMessage";
import { Client } from "./Client";
import { SocketRoom } from "./enum/SocketRoom";
import { Room } from "./Room";
import { InvalidRoomInputError } from "./error/InvalidRoomInputError";
import { ResponseMessage } from "./ResponseMessage";
import { RS_Awaiting } from "./roomState/RS_Awaiting";
import { RS_InGame } from "./roomState/RS_InGame";
import { Bot } from "./Bot";
import { Player } from "./Player";
import { Game } from "./Game";
import { DeckUpdate } from "./DeckUpdate";
import { StackUpdate } from "./StackUpdate";
import { Socket } from "socket.io";
import { UpdateActions } from "./UpdateActions";
import { CardsUpdate } from "./CardsUpdate";
import { BotActionType } from "./enum/BotActionType";
import { Card } from "./Card";
import Suit from "./enum/Suit";
import CardType from "./enum/CardType";

export class AppServer {
  readonly SOCKET: number = 1337;
  
  private server: any;
  
  private clients: ClientsContainer;
  private rooms: RoomsContainer;

  constructor() {
    this.server = io(this.SOCKET);
    
    this.clients = new ClientsContainer();
    this.rooms = new RoomsContainer();

    this.start();
  }

  private start(): void {
    this.server.httpServer.on(ServerMessage.Listening, () =>
      Logger.serverStart(this.SOCKET));

    this.server.on(ClientMessage.Connection, (socket: any) => {
      this.handleClientConnected(socket);

      socket.on(ClientMessage.Name, (name: string) =>
        this.handleClientNameChange(socket, name));

      socket.on(ClientMessage.GetRooms, () =>
        this.handleClientGetRooms(socket));

      socket.on(ClientMessage.CreateRoom, (roomObject: any) =>
        this.handleClientCreateRoom(socket, roomObject));
      socket.on(ClientMessage.JoinRoom, (roomSocket: string) =>
        this.handleClientJoinRoom(socket, roomSocket));
      socket.on(ClientMessage.GetRoomInfo, () => 
        this.handleClientGetRoomInfo(socket));
      socket.on(ClientMessage.LeaveRoom, () =>
        this.handleClientLeaveRoom(socket));
      socket.on(ClientMessage.SetReady, () =>
        this.handleClientSetReady(socket));
      socket.on(ClientMessage.AddBot, () =>
        this.handleClientAddBot(socket));
      socket.on(ClientMessage.RemoveFromRoom, (name: string) =>
        this.handleClientRemoveFromRoom(socket, name));
        
      socket.on(ClientMessage.DrawCard, () =>
        this.handleClientDrawCard(socket));
      socket.on(ClientMessage.PlayCard, (card: any) =>
        this.handleClientPlayCard(socket, card));
      socket.on(ClientMessage.PassTurn, () =>
        this.handleClientPassTurn(socket));
      
      socket.on(ClientMessage.Disconnect, () =>
        this.handleClientDisconnect(socket));
    });
  }

  private emit(receiverObject: Socket | Room | string | Client | Array<Client>, message: string | UpdateActions, data?: any): void {
    const io = this.server;
    if (Array.isArray(receiverObject)) {
      receiverObject.forEach((receiver: Client) => {
        if (message instanceof UpdateActions) {
          io.to(receiver.getSocket()).emit(ServerMessage.GameUpdate, message.getActions());
        } else {
          io.to(receiver.getSocket()).emit(message, data);
        }
      });
    } else if (receiverObject instanceof Client) {
      const socket = receiverObject.getSocket();
      if (message instanceof UpdateActions) {
        io.to(socket).emit(ServerMessage.GameUpdate, message.getActions());
      } else {
        io.to(socket).emit(message, data);
      }
    } else if (receiverObject instanceof Room) {
      const socket = SocketRoom.Room + receiverObject.getOwner().getSocket();
      if (message instanceof UpdateActions) {
        io.to(socket).emit(ServerMessage.GameUpdate, message.getActions());
      } else {
        io.to(socket).emit(message, data);
      }
    } else if (typeof receiverObject === "string") {
      if (message instanceof UpdateActions) {
        io.to(receiverObject).emit(ServerMessage.GameUpdate, message.getActions());
      } else {
        io.to(receiverObject).emit(message, data);
      }
    } else {
      if (message instanceof UpdateActions) {
        io.to(receiverObject.id).emit(ServerMessage.GameUpdate, message.getActions());
      } else {
        io.to(receiverObject.id).emit(message, data);
      }
    }
  }

  private handleClientConnected(socket: Socket): void {
    const newClient = new Client(socket.id);
    this.clients.addClient(newClient);
    Logger.clientConnected(socket.id);
  }

  private handleClientNameChange(socket: Socket, name: string): void {
    const isNameTaken: boolean = this.clients.hasClientWithName(name);
    const isNameReserved: boolean = !!Bot.NAMES.find((botName: string) => botName === name);
    const isNameOwner: boolean = !!this.rooms.getRoomsArray().find((room: Room) => room.getOwner().getName() === name);

    if (isNameTaken || isNameReserved || isNameOwner) {
      socket.emit(ServerMessage.NameIsAlreadyTaken);
      return;
    }
    
    const client = this.clients.getClient(socket.id);
    client.setName(name);
    socket.join(SocketRoom.RoomsList);
    socket.emit(ServerMessage.NameChangedSuccessfully);
    Logger.clientChangedName(socket.id, name);
  }

  private handleClientGetRooms(socket: Socket): void {
    const roomsList = this.rooms.getRoomsList();
    const client = this.clients.getClient(socket.id);
    socket.emit(ServerMessage.RoomsList, roomsList);
    Logger.clientRequestedRoomsList(socket.id, client.getName());
  }

  private handleClientCreateRoom(socket: Socket, roomObject: any): void {
    const client = this.clients.getClient(socket.id);
    const io = this.server;

    if (this.rooms.getRoom(socket.id)) {
      socket.emit(ServerMessage.ClientAlreadyOwnsARoom);
    } else {
      try {
        const room = Room.fromObject(roomObject, client);
        this.rooms.addRoom(room);
        socket.emit(ServerMessage.RoomSuccessfullyCreated, socket.id);
        io.to(SocketRoom.RoomsList).emit(ServerMessage.NewRoom, room.getGeneralInfo());
        Logger.clientCreatedRoom(socket.id, client.getName(), room.getName());
      } catch(error) {
        if (error instanceof InvalidRoomInputError) {
          socket.emit(ServerMessage.RoomInputInvalid);
          Logger.roomInputInvalid(socket.id, client.getName());
        }
      }
    }
  }

  private handleClientJoinRoom(socket: Socket, roomSocket: string): void {
    const room = this.rooms.getRoom(roomSocket);
    const client = this.clients.getClient(socket.id);
    const io = this.server;
    
    if (!room || !client) {
      return;
    }

    const response = this.addClientToRoom(client, room);

    if (response.isFailure()) {
      socket.emit(response.getMessage());
      return;
    }

    this.rooms.clearRoomTimeoutInterval(room);
    room.setState(new RS_Awaiting(room));

    socket.leave(SocketRoom.RoomsList);
    socket.join(SocketRoom.Room + roomSocket);
    socket.emit(ServerMessage.SuccessfullyJoinedRoom);
    io.to(SocketRoom.RoomsList).emit(ServerMessage.RoomUpdate, room.getGeneralInfo());
    io.to(SocketRoom.Room + roomSocket).emit(ServerMessage.RoomUpdate, room.getDetails());
    Logger.clientJoinedRoom(client, room);
  }

  private handleClientGetRoomInfo(socket: Socket): void {
    const client = this.clients.getClient(socket.id);
    const room = client.getRoom();

    socket.emit(ServerMessage.RoomUpdate, room.getDetails());
  }

  private addClientToRoom(client: Client, room: Room): ResponseMessage {
    if (client.isInRoom()) {
      return ResponseMessage.Failure(ServerMessage.ClientAlreadyInRoom);
    } else if (room.isFull()) {
      return ResponseMessage.Failure(ServerMessage.RoomIsFull);
    } else {
      client.setRoom(room);
      room.addPlayer(client);
      return ResponseMessage.Success();
    }
  }

  private handleClientLeaveRoom(socket: Socket): void {
    const client = this.clients.getClient(socket.id);
    const room = client.getRoom();
    const io = this.server;

    if (!room) {
      return;
    }

    const roomSocket = room.getOwner().getSocket();

    client.leaveRoom();

    if (room.isEmpty() || room.hasOnlyBots()) {
      this.rooms.setRoomTimeout(room, io);
      room.resetPlayersGameFlags();
    } else if (room.hasOneClientLeft()) {
      const lastClient: Client = room.getClients()[0];
      lastClient.leaveRoom();
      this.rooms.setRoomTimeout(room, io);
      io.sockets.sockets[lastClient.getSocket()].leave(SocketRoom.Room + roomSocket);
      io.sockets.sockets[lastClient.getSocket()].join(SocketRoom.RoomsList);
      io.to(lastClient.getSocket()).emit(ServerMessage.RoomSuccessfullyLeft);
      Logger.clientLeftRoom(lastClient, room);
    }

    socket.leave(SocketRoom.Room + roomSocket);
    socket.join(SocketRoom.RoomsList);
    socket.emit(ServerMessage.RoomSuccessfullyLeft);
    io.to(SocketRoom.RoomsList).emit(ServerMessage.RoomUpdate, room.getGeneralInfo());
    io.to(SocketRoom.Room + roomSocket).emit(ServerMessage.RoomUpdate, room.getDetails());
    Logger.clientLeftRoom(client, room);
  }

  private handleClientDisconnect(socket: Socket): void {
    this.handleClientLeaveRoom(socket);
    this.clients.removeClient(socket.id);
    Logger.clientDisconnected(socket.id);
  }

  private handleClientSetReady(socket: Socket): void {
    const client = this.clients.getClient(socket.id);
    const room = client.getRoom();
    const roomSocket = room.getOwner().getSocket();
    const io = this.server;

    client.setReadyToPlay(!client.isReadyToPlay());

    if (room.isReadyToStart()) {
      room.setState(new RS_InGame(room));
      io.to(SocketRoom.Room + roomSocket).emit(ServerMessage.GameStarted);
      this.handleGameStarted(room.getState().getGame());
    } else {
      socket.emit(ServerMessage.ClientIsReadyToPlay, client.isReadyToPlay());
      io.to(SocketRoom.Room + roomSocket).emit(ServerMessage.UpdateClientReadyToPlayStatus, client.getSendable());
    }
  }

  private handleClientAddBot(socket: Socket): void {
    const io = this.server;
    const client = this.clients.getClient(socket.id);
    const room = client.getRoom();
    const players = room.getPlayers();
    const roomSocket = room.getOwner().getSocket();
    const bot: Bot = Bot.Random();
    
    if (roomSocket !== socket.id) {
      return;
    }

    while (room.hasPlayerWithName(bot.getName())) {
      bot.setRandomName();
    }

    room.addPlayer(bot);
    players.forEach((player: Player) => {
      if (player instanceof Client) {
        player.setReadyToPlay(false);
        io.to(player.getSocket()).emit(ServerMessage.ClientIsReadyToPlay, player.isReadyToPlay());
      }
    });

    io.to(SocketRoom.Room + roomSocket).emit(ServerMessage.RoomUpdate, room.getDetails());
    io.to(SocketRoom.RoomsList).emit(ServerMessage.RoomUpdate, room.getGeneralInfo());
  }

  private handleClientRemoveFromRoom(socket: Socket, name: string): void {
    const client = this.clients.getClient(socket.id);
    const room = client.getRoom();
    const roomSocket = room.getOwner().getSocket();
    const io = this.server;
    
    if (roomSocket !== client.getSocket()) {
      // TODO: return rejection message
      return;
    }

    const removedPlayer = room.getPlayerByName(name);
    
    if (removedPlayer instanceof Client) {
      removedPlayer.leaveRoom();
      io.to(removedPlayer.getSocket()).emit(ServerMessage.RoomSuccessfullyLeft);
    } else {
      room.removePlayerByName(name);
    }

    io.to(SocketRoom.Room + roomSocket).emit(ServerMessage.RoomUpdate, room.getDetails());
  }

  private handleGameStarted(game: Game): void {
    const room = game.getRoom();
    const clients = room.getClients();
    const deckUpdate = DeckUpdate.Set(game.getDeck().length);
    const stackUpdate = StackUpdate.Set(game.getStack());
    const currentPlayer = game.getCurrentPlayer().object;

    game.updatePlayersCanDrawCard();
    game.updatePlayersCanPassTurn();
    game.updatePlayersCards();

    // emit data to clients
    clients.forEach((client) => {
      const actions = new UpdateActions();
      const socket = client.getSocket();
      const cards = client.getCards().map((card) => card.getSendable());

      actions.add(ServerMessage.DeckUpdate, deckUpdate);
      actions.add(ServerMessage.StackUpdate, stackUpdate);
      actions.add(ServerMessage.PlayerCards, cards);
      actions.add(ServerMessage.TurnUpdate, currentPlayer.getName());
      actions.add(ServerMessage.PlayerUpdate, client.getSendable());

      this.emit(socket, ServerMessage.GameUpdate, actions.getActions());
    });
  }

  private handleClientDrawCard(socket: Socket): void {
    const client = this.clients.getClient(socket.id);
    const room = client.getRoom();
    const otherClients = room.getClients().filter((other: Client) => other.getName() !== client.getName());
    const game = room.getState().getGame();
    const clientUpdate = new UpdateActions();
    const otherClientsUpdate = new UpdateActions();

    if (!client.hasTurn()) {
      console.log("Client doesnt have turn");
      return;
    }

    if (!game.canCurrentPlayerDrawCard()) {
      console.log("Current player cannot draw card");
      return;
    }
    
    const card = game.drawCard();
    client.addCard(card);
    client.setDrawnCard(true);
    game.updatePlayersCanDrawCard();
    game.updatePlayersCanPassTurn();
    game.updatePlayersCards();

    // construct update actions
    clientUpdate.add(ServerMessage.CardDrawn, card.getSendable());
    clientUpdate.add(ServerMessage.DeckUpdate, DeckUpdate.Remove(1));
    clientUpdate.add(ServerMessage.CardsUpdate, CardsUpdate.Add([card]));
    otherClientsUpdate.add(ServerMessage.DeckUpdate, DeckUpdate.Remove(1));

    // handle automatic pass turn if player cannot play any card
    if (!client.hasPlayableCards()) {
      this.emit(client, ServerMessage.GameUpdate, clientUpdate.getActions());
      this.emit(otherClients, ServerMessage.GameUpdate, otherClientsUpdate.getActions());

      this.handlePlayerPassTurn(game);
    } else {
      clientUpdate.add(ServerMessage.PlayerUpdate, client.getSendable());

      this.emit(client, ServerMessage.GameUpdate, clientUpdate.getActions());
      this.emit(otherClients, ServerMessage.GameUpdate, otherClientsUpdate.getActions());
    }
  }

  private handleClientPlayCard(socket: Socket, cardObject: any): void {
    const client = this.clients.getClient(socket.id);
    const room = client.getRoom();
    const game = room.getState().getGame();
    const card = client.getCardByObject(cardObject);

    if (card.isType(CardType.SuitChanging)) {
      card.changeSuit(Suit.fromObject(cardObject.changedSuit));
    } else if (card.isType(CardType.RankDemanding)) {
      card.setRankDemand(cardObject.rankDemand);
    }
    
    if (!client.hasTurn()) {
      console.log("Client doesn't have turn");
      return;
    }
    
    if (!game.isCardPlayable(card)) {
      console.log("Card is not playable");
      return;
    }

    this.handlePlayerPlayCard(game, card);
  }

  private handleClientPassTurn(socket: Socket): void {
    const client = this.clients.getClient(socket.id);
    const room = client.getRoom();
    const game = room.getState().getGame();
    this.handlePlayerPassTurn(game);
  }

  private handlePlayerPassTurn(game: Game): void {
    const player = game.getCurrentPlayer().object;
    const room = game.getRoom();
    const allClients = room.getClients();
    
    if (!player.hasTurn()) {
      console.log("current player doesn't have turn");
      return;
    }

    const previousPlayer = game.getCurrentPlayer().object;
    game.passTurn();
    game.updatePlayersCards();
    game.updatePlayersCanPassTurn();
    game.updatePlayersCanDrawCard();
    const currentPlayer = game.getCurrentPlayer().object;

    // if previous player won
    this.handlePlayerWin(previousPlayer, game);

    // if game is over
    if (game.isOver()) {
      room.setState(new RS_Awaiting(room));
      room.removeBots();
      room.resetPlayersGameFlags();
      
      // create server update action for winning and game-over
      allClients.forEach((client: Client) => {
        const updateActions = new UpdateActions();
        
        updateActions.add(ServerMessage.GameOver);

        this.emit(client, updateActions);
      });

      this.server.to(SocketRoom.RoomsList).emit(ServerMessage.RoomUpdate, room.getGeneralInfo());
      return;
    }

    // if deck is empty - move cards from stack to deck
    this.handleEmptyDeck(game);

    // if current player skips turn
    if (currentPlayer.skipsTurn()) {
      currentPlayer.setHasTurn(false);
      allClients.forEach((client: Client) => {
        const clientUpdate = new UpdateActions();
      
        clientUpdate.add(ServerMessage.TurnUpdate, currentPlayer.getName() + " ("+ currentPlayer.getTurnsToSkip() + ")");
        clientUpdate.add(ServerMessage.PlayerUpdate, client.getSendable());
  
        this.emit(client, ServerMessage.GameUpdate, clientUpdate.getActions());
      });
      currentPlayer.setHasTurn(true);

      this.handlePlayerPassTurn(game);
      
      return;
    }

    // if previous player has cards to draw
    if (previousPlayer.hasCardsToDraw()) {
      while (previousPlayer.hasCardsToDraw()) {
        const cardsToDraw = previousPlayer.getCardsToDraw();
        
        const card = game.drawCard();
        if (!card) {
          break;
        }

        previousPlayer.addCard(card);
        previousPlayer.setCardsToDraw(Math.max(cardsToDraw - 1, 0));
        
        game.updatePlayersCanDrawCard();
        game.updatePlayersCanPassTurn();
        game.updatePlayersCards();

        allClients.forEach((client: Client) => {
          const updateActions = new UpdateActions();

          updateActions.add(ServerMessage.DeckUpdate, DeckUpdate.Remove(1));
          updateActions.add(ServerMessage.CardsUpdate, CardsUpdate.Set(client.getCards()));
          updateActions.add(ServerMessage.PlayerUpdate, client.getSendable());

          this.emit(client, updateActions);
        });
      }
    }

    // emit messages to all clients
    allClients.forEach((client: Client) => {
      const clientUpdate = new UpdateActions();
      
      clientUpdate.add(ServerMessage.CardsUpdate, CardsUpdate.Set(client.getCards()));
      clientUpdate.add(ServerMessage.TurnUpdate, currentPlayer.getName());
      clientUpdate.add(ServerMessage.PlayerUpdate, client.getSendable());

      this.emit(client, ServerMessage.GameUpdate, clientUpdate.getActions());
    });

    // if current player is bot - handle it
    this.handleBotTurn(game);
  }

  private handlePlayerWin(player: Player, game: Game): void {
    if (!player.hasCards()) {
      player.setWinner(true);

      const room = game.getRoom();
      const allClients = room.getClients();
      const updateActions = new UpdateActions();

      updateActions.add(ServerMessage.Win, player.getName());

      this.emit(allClients, updateActions);
    }
  }

  private handleEmptyDeck(game: Game): void {
    if (!game.isDeckEmpty()) {
      return;
    }

    const room = game.getRoom();
    const clients = room.getClients();
    const stackLength = game.getStack().length - 1;

    game.moveStackToDeck();

    const newStackLength = game.getStack().length - 1;
    if (newStackLength < stackLength) {
      // construct update actions
      const updateActions = new UpdateActions();
      updateActions.add(ServerMessage.StackUpdate, StackUpdate.Remove());
      updateActions.add(ServerMessage.DeckUpdate, DeckUpdate.Add(stackLength));

      // emit game update to clients
      this.emit(clients, ServerMessage.GameUpdate, updateActions.getActions());
    } else {
      const currentPlayer = game.getCurrentPlayer().object;
      currentPlayer.setDrawnCard(true);
    }
  }

  private handleBotTurn(game: Game): void {
    const bot = game.getCurrentPlayer().object;

    if (!bot.isBot() || !(bot instanceof Bot)) {
      return;
    }

    const action = bot.getAction(game);
    switch (action.getType()) {
      case BotActionType.DrawCard:
        this.handleBotDrawCard(game);
        break;
      
      case BotActionType.PlayCard:
        this.handlePlayerPlayCard(game, action.getCard());
        break;

      case BotActionType.PassTurn:
        this.handlePlayerPassTurn(game);
        break;
      
      default:
        console.log("bot action", action.getType(), "not recognized.");
        break;
    }
  }

  private handleBotDrawCard(game: Game): void {
    const room = game.getRoom();
    const clients = room.getClients();
    const bot = game.getCurrentPlayer().object;
    const updateActions = new UpdateActions();
    
    const card = game.drawCard();

    if (!card) {
      this.handlePlayerPassTurn(game);
      return;
    }

    bot.addCard(card);
    bot.setDrawnCard(true);
    game.updatePlayersCanDrawCard();
    game.updatePlayersCanPassTurn();
    game.updatePlayersCards();

    // construct update actions
    updateActions.add(ServerMessage.DeckUpdate, DeckUpdate.Remove(1));

    // emit update actions to clients
    this.emit(clients, ServerMessage.GameUpdate, updateActions.getActions());

    this.handlePlayerPassTurn(game);
  }

  private handlePlayerPlayCard(game: Game, theCard: Card): void {
    const room = game.getRoom();
    const allClients = room.getClients();
    const player = game.getCurrentPlayer().object;

    game.playCard(theCard);
    game.updatePlayersCards();
    game.updatePlayersCanPassTurn();
    game.updatePlayersCanDrawCard();
    player.removeCard(theCard);
    player.setPlayedCard(true);

    allClients.forEach((client: Client) => {
      const cards = client.getCards();
      const updateActions = new UpdateActions();
      
      updateActions.add(ServerMessage.CardsUpdate, CardsUpdate.Set(cards));
      updateActions.add(ServerMessage.StackUpdate, StackUpdate.Add([theCard]));

      // handle suit-changing card played
      if (theCard.hasChangedSuit()) {
        updateActions.add(ServerMessage.ChangeStackCardSuit);
      } else if (theCard.doesDemandRank()) {
        updateActions.add(ServerMessage.ShowCardDemand);
      }
      
      this.emit(client, updateActions);
    });
    
    this.handlePlayerPassTurn(game);
  }
}