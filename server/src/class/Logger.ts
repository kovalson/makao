import { Client } from "./Client";
import { Room } from "./Room";

function filterDateDayOrMonth(value: number): string {
  return ((value < 10) ? "0" + value.toString() : value.toString());
}

function filterMonth(value: number): string {
  return filterDateDayOrMonth(value + 1);
}

function getCurrentTimeStamp(): string {
  const date = new Date();
  
  const year = date.getFullYear();
  const month = filterMonth(date.getMonth());
  const day = filterDateDayOrMonth(date.getDate());

  const hour = filterDateDayOrMonth(date.getHours());
  const minute = filterDateDayOrMonth(date.getMinutes());
  const second = filterDateDayOrMonth(date.getSeconds());

  return (
    [
      [year.toString(), month.toString(), day.toString()].join("-"),
      [hour.toString(), minute.toString(), second.toString()].join(":")
    ].join(" ") + "\t"
  );
}

export class Logger {
  public static serverStart(serverSocket: number): void {
    Logger.print(`Server listening on socket ${serverSocket}.`);
  }

  public static clientConnected(socket: string): void {
    Logger.print(`New client connected (id: ${socket}).`);
  }

  public static clientDisconnected(socket: string): void {
    Logger.print(`Client disconnected (id: ${socket})`);
  }

  public static clientChangedName(socket: string, name: string): void {
    Logger.print(`Client (id: ${socket}) changed name to "${name}".`);
  }

  public static clientDoesNotExistOnNameChange(socket: string): void {
    Logger.print(`Client (id: ${socket}) wanted to change name but given socket does not exist in clients map.`);
  }

  public static clientRequestedRoomsList(socketId: string, name: string): void {
    Logger.print(`${name} (id: ${socketId}) requested rooms list.`);
  }

  public static clientCreatedRoom(socketId: string, name: string, roomName: string): void {
    Logger.print(`${name} (id: ${socketId}) created room named "${roomName}".`);
  }

  public static roomInputInvalid(socketId: string, name: string): void {
    Logger.print(`${name} (id: ${socketId}) tried to create a room but input was invalid.`);
  }

  public static roomTimedOut(client: Client, roomName: string): void {
    Logger.print(`Room "${roomName}" created by ${client.getName()} (id: ${client.getSocket()}) timed out.`);
  }

  public static clientJoinedRoom(client: Client, room: Room): void {
    Logger.print(`${client.getName()} (id: ${client.getSocket()}) joined room "${room.getName()}" created by ${room.getOwner().getName()}.`);
  }

  public static clientGetRoomInfo(client: Client): void {
    Logger.print(`${client.getName()} (id: ${client.getSocket()}) requested room info.`);
  }

  public static clientLeftRoom(client: Client, room: Room): void {
    Logger.print(`${client.getName()} (id: ${client.getSocket()}) left room "${room.getName()}" created by ${room.getOwner().getName()}.`);
  }

  public static print(...args: any[]) {
    console.log.apply(console.log, [getCurrentTimeStamp(), ...args]);
  }
}